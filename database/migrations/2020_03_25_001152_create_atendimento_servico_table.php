<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtendimentoServicoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atendimento_servico', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->timestamps();
            $table->datetime('hora_inicio');
            $table->datetime('hora_fim')->nullable();
            $table->time('duracao')->nullable();
            $table->string('status');

            $table->unsignedBigInteger('atendimento_id');
            $table->foreign('atendimento_id')->references('id')->on('atendimentos')->unsigned();

            $table->unsignedBigInteger('profissionalServico_id');
            $table->foreign('profissionalServico_id')->references('id')->on('profissional_servico')->unsigned();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atendimento_servico');
    }
}
