<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProdutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produtos', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->timestamps();

            $table->string('nome');
            $table->string('descricao');
            $table->boolean('venda');
            $table->boolean('consumo');
            $table->double('preco_custo');
            $table->double('preco_venda');
            $table->float('comissao');
            $table->float('estoque');

            $table->unsignedBigInteger('categoriaProduto_id');
            $table->foreign('categoriaProduto_id')->references('id')->on('categoriaProdutos')->unsigned();

            $table->unsignedBigInteger('fornecedor_id');
            $table->foreign('fornecedor_id')->references('id')->on('fornecedores')->unsigned();

            $table->unsignedBigInteger('marca_id');
            $table->foreign('marca_id')->references('id')->on('marcas')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produtos');
    }
}
