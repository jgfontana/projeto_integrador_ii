<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFornecedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedores', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->timestamps();

            $table->string('cnpj', 18);
            $table->string('inscr_estadual', 11);
            $table->string('razao_social', 100);
            $table->string('nome_fantasia', 50);
            $table->string('email');
            $table->string('fone', 14);
            $table->string('celular', 15)->nullable();
            $table->string('contato', 30)->nullable();
            $table->string('site')->nullable();
            $table->string('observacao')->nullable();
            $table->integer('numero');
            $table->string('bairro');
            $table->string('cidade', 28);
            $table->string('uf', 2);
            $table->string('cep', 9);
            $table->string('complemento', 60)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedores');
    }
}
