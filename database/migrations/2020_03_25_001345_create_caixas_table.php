<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaixasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caixas', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->timestamps();

            $table->double('valor_inicial', 9,2);
            $table->double('valor_final', 9,2);
            $table->date('data');
            $table->time('hora_abertura');
            $table->time('hora_finalizacao')->nullable();
            $table->double('suprimento')->nullable();
            $table->time('hora_suprimento')->nullable();
            $table->string('observacao')->nullable();
            $table->boolean('status');

            $table->unsignedBigInteger('caixaSangria_id')->nullable();
            $table->foreign('caixaSangria_id')->references('id')->on('caixaSangrias')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caixas');
    }
}
