<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePessoasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->timestamps();
            $table->string('nome', 100);
            $table->string('sexo', 1);
            $table->string('cpf', 14)->nullable();
            $table->date('nasc')->nullable();
            $table->string('celular', 15)->nullable();
            $table->string('fone', 15)->nullable();
            $table->string('email', 100)->nullable();
            $table->text('observacao')->nullable();
            $table->text('logradouro')->nullable();
            $table->integer('numero')->nullable();
            $table->string('bairro', 50)->nullable();
            $table->string('cidade', 32)->nullable();
            $table->string('uf', 2)->nullable();
            $table->string('cep', 9)->nullable();
            $table->string('complemento', 50)->nullable();

            $table->morphs('pessoable');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pessoas');
    }
}
