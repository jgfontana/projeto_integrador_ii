<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComissoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comissoes', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->timestamps();

            $table->date('data_inicial');
            $table->date('data_final');
            $table->double('valor_total', 9,2);
            $table->string('observacao')->nullable();
            $table->date('data_pagamento')->nullable();
            $table->boolean('status');

            $table->unsignedBigInteger('comanda_id')->nullable();
            $table->foreign('comanda_id')->references('id')->on('comandas')->unsigned();

            $table->unsignedBigInteger('ajusteComissao_id')->nullable();
            $table->foreign('ajusteComissao_id')->references('id')->on('ajusteComissoes')->unsigned();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comissoes');
    }
}
