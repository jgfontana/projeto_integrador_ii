<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComandasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comandas', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->timestamps();

            $table->float('numero');
            $table->date('data');
            $table->double('valor_total', 9,2);
            $table->double('desconto', 9,2)->nullable();
            $table->double('valor_final', 9,2)->nullable();
            $table->string('status');
            $table->double('troco')->nullable();
            $table->double('recebido')->nullable();

            $table->unsignedBigInteger('atendimento_id')->nullable();
            $table->foreign('atendimento_id')->references('id')->on('atendimentos')->unsigned();

            $table->unsignedBigInteger('cliente_id')->nullable();
            $table->foreign('cliente_id')->references('id')->on('clientes')->unsigned();

            //$table->unsignedBigInteger('profissional_id')->nullable();
            //$table->foreign('profissional_id')->references('id')->on('profissionais')->unsigned();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comandas');
    }
}
