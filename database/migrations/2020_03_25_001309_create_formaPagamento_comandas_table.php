<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormaPagamentoComandasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formaPagamento_comandas', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->id();
            $table->timestamps();
            $table->double('valor');

            $table->unsignedBigInteger('comanda_id');
            $table->foreign('comanda_id')->references('id')->on('comandas')->unsigned();

            $table->unsignedBigInteger('formaPagamento_id');
            $table->foreign('formaPagamento_id')->references('id')->on('formaPagamentos')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formaPagamento_comandas');
    }
}
