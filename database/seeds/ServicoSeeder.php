<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServicoSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'id'                    => '1',
            'nome_servico'          => 'Relaxamento Guanidina',
            'duracao'               => '01:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '290.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '2',
            'nome_servico'          => 'Blindagem siliconada',
            'duracao'               => '01:20:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '240.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '3',
            'nome_servico'          => 'Escova 3D',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '210.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '4',
            'nome_servico'          => 'Escova de chocolate',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '185.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '5',
            'nome_servico'          => 'Tintura base',
            'duracao'               => '00:45:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '95.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '6',
            'nome_servico'          => 'Retoque de raiz',
            'duracao'               => '00:45:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '80.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '7',
            'nome_servico'          => 'Luzes',
            'duracao'               => '03:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '95.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '8',
            'nome_servico'          => 'Ballayage',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '115.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '9',
            'nome_servico'          => 'Acetinagem',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '95.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '10',
            'nome_servico'          => 'Corte masculino',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '28.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '11',
            'nome_servico'          => 'Corte feminino',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '40.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '12',
            'nome_servico'          => 'Tratamento capilar',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '50.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '13',
            'nome_servico'          => 'Hidratação capilar (Sleek)',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '50.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '14',
            'nome_servico'          => 'Hidratação com cápsula',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '60.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '15',
            'nome_servico'          => 'Hidrat. de queratina 3 min',
            'duracao'               => '00:03:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '45.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '16',
            'nome_servico'          => 'Hidratação Bonacure',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '45.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '17',
            'nome_servico'          => 'Lavagem de cabelo',
            'duracao'               => '00:20:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '15.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '18',
            'nome_servico'          => 'Lavar e secar',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '20.00',
            //'comissao'              => '10',
            'categoria_id'          => '1',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '19',
            'nome_servico'          => 'Maquiagem',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '70.00',
            //'comissao'              => '10',
            'categoria_id'          => '2',
            'status'                => 'A'
        ]); 

        DB::table('servicos')->insert([
            'id'                    => '20',
            'nome_servico'          => 'Aplicação de cílios',
            'duracao'               => '02:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '200.00',
            //'comissao'              => '10',
            'categoria_id'          => '2',
            'status'                => 'A'
        ]); 

        DB::table('servicos')->insert([
            'id'                    => '21',
            'nome_servico'          => 'Penteado',
            'duracao'               => '00:20:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '55.00',
            //'comissao'              => '10',
            'categoria_id'          => '2',
            'status'                => 'A'
        ]); 

        DB::table('servicos')->insert([
            'id'                    => '22',
            'nome_servico'          => 'Escova',
            'duracao'               => '00:20:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '50.00',
            //'comissao'              => '10',
            'categoria_id'          => '2',
            'status'                => 'A'
        ]); 

        DB::table('servicos')->insert([
            'id'                    => '23',
            'nome_servico'          => 'Photon Dome',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '50.00',
            //'comissao'              => '10',
            'categoria_id'          => '3',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '24',
            'nome_servico'          => 'Photon Roller',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '50.00',
            //'comissao'              => '10',
            'categoria_id'          => '3',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '25',
            'nome_servico'          => 'Relaxante',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '40.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);   

        DB::table('servicos')->insert([
            'id'                    => '26',
            'nome_servico'          => 'Reflexoterapia podal',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '30.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '27',
            'nome_servico'          => 'Drenagem linfática',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '55.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '28',
            'nome_servico'          => 'Esfoliação corporal com hidratação',
            'duracao'               => '00:35:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '60.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '29',
            'nome_servico'          => 'Terapia de pedras quentes',
            'duracao'               => '00:35:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '60.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '30',
            'nome_servico'          => 'Relaxante c/ aplic. de pedras quentes',
            'duracao'               => '00:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '50.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '31',
            'nome_servico'          => 'Banho de imersão',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '65.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '32',
            'nome_servico'          => 'Banho de lua (fio dourado)',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '50.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '33',
            'nome_servico'          => 'Perna inteira',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '55.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '34',
            'nome_servico'          => 'Meia perna',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '15.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);  

        DB::table('servicos')->insert([
            'id'                    => '35',
            'nome_servico'          => 'Braço',
            'duracao'               => '00:15:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '30.00',
            //'comissao'              => '10',
            'categoria_id'          => '4',
            'status'                => 'A'
        ]);
  

        DB::table('servicos')->insert([
            'id'                    => '36',
            'nome_servico'          => 'Limpeza de pele profunda',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '70.00',
            //'comissao'              => '10',
            'categoria_id'          => '5',
            'status'                => 'A'
        ]);
  

        DB::table('servicos')->insert([
            'id'                    => '37',
            'nome_servico'          => 'Peeling (pele com acne)',
            'duracao'               => '01:20:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '45.00',
            //'comissao'              => '10',
            'categoria_id'          => '5',
            'status'                => 'A'
        ]);
  

        DB::table('servicos')->insert([
            'id'                    => '38',
            'nome_servico'          => 'Peeling rejuvenescimento',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '45.00',
            //'comissao'              => '10',
            'categoria_id'          => '5',
            'status'                => 'A'
        ]);
  

        DB::table('servicos')->insert([
            'id'                    => '39',
            'nome_servico'          => 'Peeling clareamento',
            'duracao'               => '01:00:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '45.00',
            //'comissao'              => '10',
            'categoria_id'          => '5',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '40',
            'nome_servico'          => 'Design de sobrancelha',
            'duracao'               => '00:15:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '20.00',
            //'comissao'              => '10',
            'categoria_id'          => '6',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '41',
            'nome_servico'          => 'Pintura de sobrancelhas c/ design',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '28.00',
            //'comissao'              => '10',
            'categoria_id'          => '6',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '42',
            'nome_servico'          => 'Pintura de cílios',
            'duracao'               => '00:10:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '22.00',
            //'comissao'              => '10',
            'categoria_id'          => '6',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '43',
            'nome_servico'          => 'Mãos',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '17.00',
            //'comissao'              => '10',
            'categoria_id'          => '7',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '44',
            'nome_servico'          => 'Pés',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '20.00',
            //'comissao'              => '10',
            'categoria_id'          => '7',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '45',
            'nome_servico'          => 'Hidratação com parafina para mãos',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '25.00',
            //'comissao'              => '10',
            'categoria_id'          => '7',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '46',
            'nome_servico'          => 'Hidratação com parafina para pés',
            'duracao'               => '00:30:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '35.00',
            //'comissao'              => '10',
            'categoria_id'          => '7',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '47',
            'nome_servico'          => 'Perna inteira',
            'duracao'               => '00:20:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '38.00',
            //'comissao'              => '10',
            'categoria_id'          => '8',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '48',
            'nome_servico'          => 'Buço',
            'duracao'               => '00:10:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '13.00',
            //'comissao'              => '10',
            'categoria_id'          => '8',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '49',
            'nome_servico'          => 'Meia perna',
            'duracao'               => '00:10:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '28.00',
            //'comissao'              => '10',
            'categoria_id'          => '8',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '50',
            'nome_servico'          => 'Virilha',
            'duracao'               => '00:20:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '23.00',
            //'comissao'              => '10',
            'categoria_id'          => '8',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '51',
            'nome_servico'          => 'Axila',
            'duracao'               => '00:10:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '17.00',
            //'comissao'              => '10',
            'categoria_id'          => '8',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '52',
            'nome_servico'          => 'Barba Simples',
            'duracao'               => '00:10:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '25.00',
            //'comissao'              => '10',
            'categoria_id'          => '9',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '53',
            'nome_servico'          => 'Barba Pigmentanda',
            'duracao'               => '00:40:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '35.00',
            //'comissao'              => '10',
            'categoria_id'          => '9',
            'status'                => 'A'
        ]);

        DB::table('servicos')->insert([
            'id'                    => '54',
            'nome_servico'          => 'Barba com toalha quente',
            'duracao'               => '00:20:00',
            'descricao'             => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam interdum.',
            'preco'                 => '35.00',
            //'comissao'              => '10',
            'categoria_id'          => '9',
            'status'                => 'A'
        ]);
    }
}
