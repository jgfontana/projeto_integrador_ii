<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FormasPagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formaPagamentos')->insert([
            'id'    => '1',
            'nome'  => 'Dinheiro',
            'status'=> 'A'
        ]);

        DB::table('formaPagamentos')->insert([
            'id'    => '2',
            'nome'  => 'Cartão de Débito',
            'status'=> 'A'
        ]);

        DB::table('formaPagamentos')->insert([
            'id'    => '3',
            'nome'  => 'Cartão de Crédito',
            'status'=> 'A'
        ]);

        DB::table('formaPagamentos')->insert([
            'id'    => '4',
            'nome'  => 'Cheque',
            'status'=> 'A'
        ]);

        DB::table('formaPagamentos')->insert([
            'id'    => '5',
            'nome'  => 'Vale Presente',
            'status'=> 'A'
        ]);
    }
}
