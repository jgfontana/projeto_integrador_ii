<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AtendimentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('atendimentos')->insert([
            'id'                => '1',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'F',
            'cliente_id'        => '1',
            'comandagerada'     => 'S'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '2',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'F',
            'cliente_id'        => '2',
            'comandagerada'     => 'S'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '3',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'F',
            'cliente_id'        => '3',
            'comandagerada'     => 'S'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '4',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'F',
            'cliente_id'        => '4',
            'comandagerada'     => 'S'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '5',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'F',
            'cliente_id'        => '5',
            'comandagerada'     => 'S'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '6',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'F',
            'cliente_id'        => '6',
            'comandagerada'     => 'S'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '7',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'F',
            'cliente_id'        => '7',
            'comandagerada'     => 'S'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '8',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '8',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '9',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '9',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '10',
            'data'              => '2020-06-12',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '10',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '11',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '11',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '12',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '12',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '13',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '13',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '14',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '14',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '15',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '15',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '16',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '16',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '17',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '17',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '18',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '18',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '19',
            'data'              => '2020-06-08',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '19',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '20',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '20',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '21',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '21',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '22',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '22',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '23',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '23',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '24',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '24',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '25',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '25',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '26',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '26',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '27',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '27',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '28',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '28',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '29',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '29',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '30',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '30',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '31',
            'data'              => '2020-06-09',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '31',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '32',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '32',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '33',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '33',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '34',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '34',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '35',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '35',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '36',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '36',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '37',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '37',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '38',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '38',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '39',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '39',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '40',
            'data'              => '2020-06-10',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '40',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '41',
            'data'              => '2020-06-11',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '41',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '42',
            'data'              => '2020-06-11',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '42',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '43',
            'data'              => '2020-06-11',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '43',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '44',
            'data'              => '2020-06-11',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '44',
            'comandagerada'     => 'N'
        ]);

        DB::table('atendimentos')->insert([
            'id'                => '45',
            'data'              => '2020-06-11',
            'observacao'        => '',
            'status'            => 'A',
            'cliente_id'        => '45',
            'comandagerada'     => 'N'
        ]);
    }
}
