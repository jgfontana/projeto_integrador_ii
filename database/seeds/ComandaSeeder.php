<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ComandaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comandas')->insert([
            'id'                => '1',
            'numero'            => '1',
            'data'              => '2020-06-12',
            'valor_total'       => '210.00',
            'status'            => 'A',
            'atendimento_id'    => '1',
            'cliente_id'        => '1',
        ]);

        DB::table('comandas')->insert([
            'id'                => '2',
            'numero'            => '2',
            'data'              => '2020-06-12',
            'valor_total'       => '30.00',
            'status'            => 'A',
            'atendimento_id'    => '2',
            'cliente_id'        => '2',
        ]);

        DB::table('comandas')->insert([
            'id'                => '3',
            'numero'            => '3',
            'data'              => '2020-06-12',
            'valor_total'       => '20.00',
            'status'            => 'A',
            'atendimento_id'    => '3',
            'cliente_id'        => '3',
        ]);

        DB::table('comandas')->insert([
            'id'                => '4',
            'numero'            => '4',
            'data'              => '2020-06-12',
            'valor_total'       => '45.00',
            'status'            => 'A',
            'atendimento_id'    => '4',
            'cliente_id'        => '4',
        ]);

        DB::table('comandas')->insert([
            'id'                => '5',
            'numero'            => '5',
            'data'              => '2020-06-12',
            'valor_total'       => '28.00',
            'status'            => 'A',
            'atendimento_id'    => '5',
            'cliente_id'        => '5',
        ]);

        DB::table('comandas')->insert([
            'id'                => '6',
            'numero'            => '6',
            'data'              => '2020-06-12',
            'valor_total'       => '23.00',
            'status'            => 'A',
            'atendimento_id'    => '6',
            'cliente_id'        => '6',
        ]);

        DB::table('comandas')->insert([
            'id'                => '7',
            'numero'            => '7',
            'data'              => '2020-06-12',
            'valor_total'       => '20.00',
            'status'            => 'A',
            'atendimento_id'    => '7',
            'cliente_id'        => '7',
        ]);
    }
}
