<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfissionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profissionais')->insert([
            'id'            => '1',
            //'comissionado'  => true,
            'cor'           => '#009933',
            'status'        => 'A'
        ]);

        DB::table('profissionais')->insert([
            'id'            => '2',
            //'comissionado'  => true,
            'cor'           => '#cc0000',
            'status'        => 'A'
        ]);

        DB::table('profissionais')->insert([
            'id'            => '3',
            //'comissionado'  => true,
            'cor'           => '#ff33cc',
            'status'        => 'A'
        ]);

        DB::table('profissionais')->insert([
            'id'            => '4',
            //'comissionado'  => true,
            'cor'           => '#0066cc',
            'status'        => 'A'
        ]);

        DB::table('profissionais')->insert([
            'id'            => '5',
            //'comissionado'  => true,
            'cor'           => '#ff9900',
            'status'        => 'A'
        ]);

        DB::table('profissionais')->insert([
            'id'            => '6',
            //'comissionado'  => true,
            'cor'           => '#9933ff',
            'status'        => 'A'
        ]);

        DB::table('profissionais')->insert([
            'id'            => '7',
            //'comissionado'  => false,
            'cor'           => '#993366',
            'status'        => 'A'
        ]);

        DB::table('profissionais')->insert([
            'id'            => '8',
            //'comissionado'  => false,
            'cor'           => '#cc3300',
            'status'        => 'A'
        ]);

        DB::table('profissionais')->insert([
            'id'            => '9',
            //'comissionado'  => false,
            'cor'           => '#0099cc',
            'status'        => 'A'
        ]);
    }
}
