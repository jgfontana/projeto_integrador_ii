<?php

use Illuminate\Database\Seeder;

class ServicocategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('servicocategorias')->insert([
            'id'            => '1',
            'nome'          => 'Cabelo',
            'descricao'     => ''
        ]);

        DB::table('servicocategorias')->insert([
            'id'            => '2',
            'nome'          => 'Produção visual',
            'descricao'     => ''
        ]);

        DB::table('servicocategorias')->insert([
            'id'            => '3',
            'nome'          => 'Corporal',
            'descricao'     => ''
        ]);

        DB::table('servicocategorias')->insert([
            'id'            => '4',
            'nome'          => 'Massagem',
            'descricao'     => ''
        ]);

        DB::table('servicocategorias')->insert([
            'id'            => '5',
            'nome'          => 'Facial',
            'descricao'     => ''
        ]);

        DB::table('servicocategorias')->insert([
            'id'            => '6',
            'nome'          => 'Olhos',
            'descricao'     => ''
        ]);

        DB::table('servicocategorias')->insert([
            'id'            => '7',
            'nome'          => 'Manicure/Pedicure',
            'descricao'     => ''
        ]);

        DB::table('servicocategorias')->insert([
            'id'            => '8',
            'nome'          => 'Depilação',
            'descricao'     => ''
        ]);

        DB::table('servicocategorias')->insert([
            'id'            => '9',
            'nome'          => 'Barba',
            'descricao'     => ''
        ]);
    }
}
