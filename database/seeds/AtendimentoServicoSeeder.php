<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AtendimentoServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('atendimento_servico')->insert([
            'id'                        => '1',
            'profissionalServico_id'    => '7',
            'atendimento_id'            => '1',
            'hora_inicio'               => '2020-06-12 08:30:00',
            'hora_fim'                  => '2020-06-12 09:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'F'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '2',
            'profissionalServico_id'    => '30',
            'atendimento_id'            => '2',
            'hora_inicio'               => '2020-06-12 09:00:00',
            'hora_fim'                  => '2020-06-12 09:30:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'F'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '3',
            'profissionalServico_id'    => '22',
            'atendimento_id'            => '3',
            'hora_inicio'               => '2020-06-12 09:30:00',
            'hora_fim'                  => '2020-06-12 10:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'F'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '4',
            'profissionalServico_id'    => '41',
            'atendimento_id'            => '4',
            'hora_inicio'               => '2020-06-12 10:00:00',
            'hora_fim'                  => '2020-06-12 11:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'F'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '5',
            'profissionalServico_id'    => '45',
            'atendimento_id'            => '5',
            'hora_inicio'               => '2020-06-12 11:30:00',
            'hora_fim'                  => '2020-06-12 12:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'F'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '6',
            'profissionalServico_id'    => '54',
            'atendimento_id'            => '6',
            'hora_inicio'               => '2020-06-12 13:30:00',
            'hora_fim'                  => '2020-06-12 14:30:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'F'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '7',
            'profissionalServico_id'    => '44',
            'atendimento_id'            => '7',
            'hora_inicio'               => '2020-06-12 15:00:00',
            'hora_fim'                  => '2020-06-12 15:30:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'F'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '8',
            'profissionalServico_id'    => '35',
            'atendimento_id'            => '8',
            'hora_inicio'               => '2020-06-12 15:30:00',
            'hora_fim'                  => '2020-06-12 16:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '9',
            'profissionalServico_id'    => '18',
            'atendimento_id'            => '9',
            'hora_inicio'               => '2020-06-12 16:00:00',
            'hora_fim'                  => '2020-06-12 17:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '10',
            'profissionalServico_id'    => '28',
            'atendimento_id'            => '10',
            'hora_inicio'               => '2020-06-12 17:30:00',
            'hora_fim'                  => '2020-06-12 18:30:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '11',
            'profissionalServico_id'    => '37',
            'atendimento_id'            => '11',
            'hora_inicio'               => '2020-06-08 09:00:00',
            'hora_fim'                  => '2020-06-08 10:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '12',
            'profissionalServico_id'    => '14',
            'atendimento_id'            => '12',
            'hora_inicio'               => '2020-06-08 10:00:00',
            'hora_fim'                  => '2020-06-08 10:30:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '13',
            'profissionalServico_id'    => '25',
            'atendimento_id'            => '13',
            'hora_inicio'               => '2020-06-08 10:30:00',
            'hora_fim'                  => '2020-06-08 11:30:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '14',
            'profissionalServico_id'    => '15',
            'atendimento_id'            => '14',
            'hora_inicio'               => '2020-06-08 11:30:00',
            'hora_fim'                  => '2020-06-08 12:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '15',
            'profissionalServico_id'    => '51',
            'atendimento_id'            => '15',
            'hora_inicio'               => '2020-06-08 13:30:00',
            'hora_fim'                  => '2020-06-08 14:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '16',
            'profissionalServico_id'    => '12',
            'atendimento_id'            => '16',
            'hora_inicio'               => '2020-06-08 14:15:00',
            'hora_fim'                  => '2020-06-08 15:00:00',
            'duracao'                   => '00:45:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '17',
            'profissionalServico_id'    => '39',
            'atendimento_id'            => '17',
            'hora_inicio'               => '2020-06-08 15:00:00',
            'hora_fim'                  => '2020-06-08 16:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '18',
            'profissionalServico_id'    => '16',
            'atendimento_id'            => '18',
            'hora_inicio'               => '2020-06-08 16:15:00',
            'hora_fim'                  => '2020-06-08 17:00:00',
            'duracao'                   => '00:45:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '19',
            'profissionalServico_id'    => '24',
            'atendimento_id'            => '19',
            'hora_inicio'               => '2020-06-08 17:00:00',
            'hora_fim'                  => '2020-06-08 18:30:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '20',
            'profissionalServico_id'    => '26',
            'atendimento_id'            => '20',
            'hora_inicio'               => '2020-06-09 08:00:00',
            'hora_fim'                  => '2020-06-09 08:30:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '21',
            'profissionalServico_id'    => '51',
            'atendimento_id'            => '21',
            'hora_inicio'               => '2020-06-09 09:00:00',
            'hora_fim'                  => '2020-06-09 10:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '22',
            'profissionalServico_id'    => '31',
            'atendimento_id'            => '22',
            'hora_inicio'               => '2020-06-09 10:00:00',
            'hora_fim'                  => '2020-06-09 10:30:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '23',
            'profissionalServico_id'    => '37',
            'atendimento_id'            => '23',
            'hora_inicio'               => '2020-06-09 10:30:00',
            'hora_fim'                  => '2020-06-09 11:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '24',
            'profissionalServico_id'    => '43',
            'atendimento_id'            => '24',
            'hora_inicio'               => '2020-06-09 11:00:00',
            'hora_fim'                  => '2020-06-09 12:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '25',
            'profissionalServico_id'    => '55',
            'atendimento_id'            => '25',
            'hora_inicio'               => '2020-06-09 13:10:00',
            'hora_fim'                  => '2020-06-09 14:00:00',
            'duracao'                   => '00:50:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '26',
            'profissionalServico_id'    => '7',
            'atendimento_id'            => '26',
            'hora_inicio'               => '2020-06-09 14:00:00',
            'hora_fim'                  => '2020-06-09 15:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '27',
            'profissionalServico_id'    => '23',
            'atendimento_id'            => '27',
            'hora_inicio'               => '2020-06-09 15:00:00',
            'hora_fim'                  => '2020-06-09 15:40:00',
            'duracao'                   => '00:40:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '28',
            'profissionalServico_id'    => '52',
            'atendimento_id'            => '28',
            'hora_inicio'               => '2020-06-09 16:20:00',
            'hora_fim'                  => '2020-06-09 17:00:00',
            'duracao'                   => '00:40:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '29',
            'profissionalServico_id'    => '19',
            'atendimento_id'            => '29',
            'hora_inicio'               => '2020-06-09 17:00:00',
            'hora_fim'                  => '2020-06-09 17:30:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '30',
            'profissionalServico_id'    => '32',
            'atendimento_id'            => '30',
            'hora_inicio'               => '2020-06-09 17:30:00',
            'hora_fim'                  => '2020-06-09 18:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '31',
            'profissionalServico_id'    => '39',
            'atendimento_id'            => '31',
            'hora_inicio'               => '2020-06-09 18:00:00',
            'hora_fim'                  => '2020-06-09 19:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '32',
            'profissionalServico_id'    => '27',
            'atendimento_id'            => '32',
            'hora_inicio'               => '2020-06-10 08:00:00',
            'hora_fim'                  => '2020-06-10 09:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '33',
            'profissionalServico_id'    => '34',
            'atendimento_id'            => '33',
            'hora_inicio'               => '2020-06-10 09:00:00',
            'hora_fim'                  => '2020-06-10 10:30:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '34',
            'profissionalServico_id'    => '25',
            'atendimento_id'            => '34',
            'hora_inicio'               => '2020-06-10 10:30:00',
            'hora_fim'                  => '2020-06-10 11:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '35',
            'profissionalServico_id'    => '26',
            'atendimento_id'            => '35',
            'hora_inicio'               => '2020-06-10 11:00:00',
            'hora_fim'                  => '2020-06-10 12:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '36',
            'profissionalServico_id'    => '14',
            'atendimento_id'            => '36',
            'hora_inicio'               => '2020-06-10 13:20:00',
            'hora_fim'                  => '2020-06-10 14:00:00',
            'duracao'                   => '00:40:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '37',
            'profissionalServico_id'    => '33',
            'atendimento_id'            => '37',
            'hora_inicio'               => '2020-06-10 14:00:00',
            'hora_fim'                  => '2020-06-10 15:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '38',
            'profissionalServico_id'    => '11',
            'atendimento_id'            => '38',
            'hora_inicio'               => '2020-06-10 15:30:00',
            'hora_fim'                  => '2020-06-10 16:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '39',
            'profissionalServico_id'    => '45',
            'atendimento_id'            => '39',
            'hora_inicio'               => '2020-06-10 16:00:00',
            'hora_fim'                  => '2020-06-10 17:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '40',
            'profissionalServico_id'    => '35',
            'atendimento_id'            => '40',
            'hora_inicio'               => '2020-06-10 17:15:00',
            'hora_fim'                  => '2020-06-10 18:00:00',
            'duracao'                   => '00:45:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '41',
            'profissionalServico_id'    => '41',
            'atendimento_id'            => '41',
            'hora_inicio'               => '2020-06-11 09:00:00',
            'hora_fim'                  => '2020-06-11 10:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '42',
            'profissionalServico_id'    => '36',
            'atendimento_id'            => '42',
            'hora_inicio'               => '2020-06-11 10:00:00',
            'hora_fim'                  => '2020-06-11 11:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '43',
            'profissionalServico_id'    => '48',
            'atendimento_id'            => '43',
            'hora_inicio'               => '2020-06-11 11:00:00',
            'hora_fim'                  => '2020-06-11 12:00:00',
            'duracao'                   => '01:00:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '44',
            'profissionalServico_id'    => '21',
            'atendimento_id'            => '44',
            'hora_inicio'               => '2020-06-11 13:30:00',
            'hora_fim'                  => '2020-06-11 15:00:00',
            'duracao'                   => '01:30:00',
            'status'                    => 'A'
        ]);

        DB::table('atendimento_servico')->insert([
            'id'                        => '45',
            'profissionalServico_id'    => '40',
            'atendimento_id'            => '45',
            'hora_inicio'               => '2020-06-11 15:30:00',
            'hora_fim'                  => '2020-06-11 16:00:00',
            'duracao'                   => '00:30:00',
            'status'                    => 'A'
        ]);
    }
}
