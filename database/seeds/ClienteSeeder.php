<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClienteSeeder extends Seeder
{
    public function run()
    {
        DB::table('clientes')->insert([
            'id'            => '1',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '2',
            'alergia'       => true,
            'descricao'     => 'Cliente é alergica ao produto X (teste)',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '3',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '4',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '5',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '6',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '7',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '8',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '9',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '10',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '11',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '12',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '13',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '14',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '15',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '16',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '17',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '18',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '19',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '20',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '21',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '22',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '23',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '24',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '25',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '26',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '27',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '28',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '29',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '30',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '31',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '32',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '33',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '34',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '35',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '36',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '37',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '38',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '39',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '40',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '41',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '42',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '43',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '44',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '45',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '46',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '47',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '48',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '49',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '50',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '51',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '52',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '53',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '54',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '55',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);

        DB::table('clientes')->insert([
            'id'            => '56',
            'alergia'       => false,
            'descricao'     => '',
            'status'        => 'A'
        ]);
    }
}
