<?php

use App\FormaPagamento;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ClienteSeeder::class,
            ProfissionalSeeder::class,
            PessoaSeeder::class,
            ServicocategoriaSeeder::class,
            ServicoSeeder::class,
            ProfissionalServicoSeeder::class,
            //AtendimentoSeeder::class,
            //AtendimentoServicoSeeder::class,
            //ComandaSeeder::class,
            FormasPagamentoSeeder::class
        ]);
    }
}
