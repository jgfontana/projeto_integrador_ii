<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProfissionalServicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profissional_servico')->insert([
            'id'                => '1',
            'profissional_id'   => '1',
            'servico_id'        => '52',
            'status'            => 'A',
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '2',
            'profissional_id'   => '1',
            'servico_id'        => '53',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '3',
            'profissional_id'   => '1',
            'servico_id'        => '54',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '4',
            'profissional_id'   => '1',
            'servico_id'        => '10',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '5',
            'profissional_id'   => '2',
            'servico_id'        => '1',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '6',
            'profissional_id'   => '2',
            'servico_id'        => '2',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '7',
            'profissional_id'   => '2',
            'servico_id'        => '3',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '8',
            'profissional_id'   => '2',
            'servico_id'        => '4',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '9',
            'profissional_id'   => '2',
            'servico_id'        => '5',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '10',
            'profissional_id'   => '2',
            'servico_id'        => '6',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '11',
            'profissional_id'   => '2',
            'servico_id'        => '7',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '12',
            'profissional_id'   => '2',
            'servico_id'        => '8',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '13',
            'profissional_id'   => '2',
            'servico_id'        => '9',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '14',
            'profissional_id'   => '2',
            'servico_id'        => '10',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '15',
            'profissional_id'   => '2',
            'servico_id'        => '11',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '16',
            'profissional_id'   => '2',
            'servico_id'        => '12',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '17',
            'profissional_id'   => '2',
            'servico_id'        => '13',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '18',
            'profissional_id'   => '2',
            'servico_id'        => '14',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '19',
            'profissional_id'   => '2',
            'servico_id'        => '15',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '20',
            'profissional_id'   => '2',
            'servico_id'        => '16',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '21',
            'profissional_id'   => '2',
            'servico_id'        => '17',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '22',
            'profissional_id'   => '2',
            'servico_id'        => '18',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '23',
            'profissional_id'   => '3',
            'servico_id'        => '19',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '24',
            'profissional_id'   => '3',
            'servico_id'        => '20',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '25',
            'profissional_id'   => '3',
            'servico_id'        => '21',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '26',
            'profissional_id'   => '3',
            'servico_id'        => '22',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '27',
            'profissional_id'   => '4',
            'servico_id'        => '23',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '28',
            'profissional_id'   => '4',
            'servico_id'        => '24',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '29',
            'profissional_id'   => '5',
            'servico_id'        => '25',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '30',
            'profissional_id'   => '5',
            'servico_id'        => '26',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '31',
            'profissional_id'   => '5',
            'servico_id'        => '27',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '32',
            'profissional_id'   => '5',
            'servico_id'        => '28',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '33',
            'profissional_id'   => '5',
            'servico_id'        => '29',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '34',
            'profissional_id'   => '5',
            'servico_id'        => '30',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '35',
            'profissional_id'   => '5',
            'servico_id'        => '31',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '36',
            'profissional_id'   => '5',
            'servico_id'        => '32',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '37',
            'profissional_id'   => '5',
            'servico_id'        => '33',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '38',
            'profissional_id'   => '5',
            'servico_id'        => '34',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '39',
            'profissional_id'   => '5',
            'servico_id'        => '35',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '40',
            'profissional_id'   => '6',
            'servico_id'        => '36',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '41',
            'profissional_id'   => '6',
            'servico_id'        => '37',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '42',
            'profissional_id'   => '6',
            'servico_id'        => '38',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '43',
            'profissional_id'   => '6',
            'servico_id'        => '39',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '44',
            'profissional_id'   => '7',
            'servico_id'        => '40',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '45',
            'profissional_id'   => '7',
            'servico_id'        => '41',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '46',
            'profissional_id'   => '7',
            'servico_id'        => '42',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '47',
            'profissional_id'   => '7',
            'servico_id'        => '43',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '48',
            'profissional_id'   => '8',
            'servico_id'        => '44',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '49',
            'profissional_id'   => '8',
            'servico_id'        => '45',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '50',
            'profissional_id'   => '8',
            'servico_id'        => '46',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '51',
            'profissional_id'   => '9',
            'servico_id'        => '47',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '52',
            'profissional_id'   => '9',
            'servico_id'        => '48',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '53',
            'profissional_id'   => '9',
            'servico_id'        => '49',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '54',
            'profissional_id'   => '9',
            'servico_id'        => '50',
            'status'            => 'A'
        ]);

        DB::table('profissional_servico')->insert([
            'id'                => '55',
            'profissional_id'   => '9',
            'servico_id'        => '51',
            'status'            => 'A'
        ]);
    }
}
