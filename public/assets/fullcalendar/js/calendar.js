document.addEventListener('DOMContentLoaded', function () {
  var Calendar = FullCalendar.Calendar;
  var Draggable = FullCalendarInteraction.Draggable

  /* initialize the external events
  -----------------------------------------------------------------*/

  var containerEl = document.getElementById('external-events-list');
  new Draggable(containerEl, {
    itemSelector: '.fc-event',
    eventData: function (eventEl) {
      return {
        title: eventEl.innerText.trim()
      }
    }
  });


  /* initialize the calendar
  -----------------------------------------------------------------*/

  var calendarEl = document.getElementById('calendar');
  var calendar = new Calendar(calendarEl, {
    plugins: ['interaction', 'dayGrid', 'timeGrid', 'list'],
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
    },
    contentHeight: "auto",
    locale: 'pt-br',
    navLinks: true,
    eventLimit: true,
    selectable: false,
    editable: false,
    droppable: false, // this allows things to be dropped onto the calendar
    drop: function (arg) {
      // is the "remove after drop" checkbox checked?
      if (document.getElementById('drop-remove').checked) {
        // if so, remove the element from the "Draggable Events" list
        arg.draggedEl.parentNode.removeChild(arg.draggedEl);
      }
    },
    /*eventDrop: function(event){
      alert('Event Drop');
    },*/
    eventClick: function (element) {
      $('#EditAgendamento').modal('show');
      $('#EditAgendamento #save').show();
        $('#EditAgendamento #delete').show();
        $('#EditAgendamento #fin').show();
      
      let id = element.event.id;
      $('#EditAgendamento #id').val(id);

      let atendimento_servico_id = element.event.extendedProps.atendimento_servico_id;
      $('#EditAgendamento #atendimento_servico_id').val(atendimento_servico_id);
      $('#EditAgendamento #delete').val(atendimento_servico_id);

      let title = element.event.title;
      $('#EditAgendamento #exampleModalLabel').text('#'+id+' - Atendimento de: ' + title);

      let cliente_id = element.event.extendedProps.cliente_id;
      $('#EditAgendamento .cliente_id').selectpicker('val', cliente_id);
      $('#EditAgendamento .cliente_id').attr('style', 'pointer-events: none;');
      $('#EditAgendamento .cliente_id').attr('disabled', 'true');

      let data = moment(element.event.start).format("YYYY-MM-DD");
      $('#EditAgendamento .data').val(data);

      let duracao = element.event.extendedProps.duracao;
      $('#EditAgendamento .aux_duracao').val(duracao);

      let servico_id = element.event.extendedProps.servico_id;
      $('#EditAgendamento .servico').selectpicker('val', servico_id);
      $('#EditAgendamento .servico').change();
      $('#EditAgendamento .servico').attr('style', 'pointer-events: none;');

      let hora_inicio = moment(element.event.start).format("HH:mm");
      $('#EditAgendamento .hora_inicio1').val(hora_inicio);

      let obs = element.event.extendedProps.obs;
      $('#EditAgendamento .observacao').val(obs);

      //* se for finalizado:
      let status = element.event.extendedProps.status;
      if (status == 'F'){
        $('#EditAgendamento #exampleModalLabel').text('#'+id+' - Atendimento de: ' + title + ' (FINALIZADO)');
        $('#EditAgendamento #save').hide();
        $('#EditAgendamento #delete').hide();
        $('#EditAgendamento #fin').hide();
      }
    },
  });
  calendar.render();
  $('.select-profissional-ag').change(function () {
    calendar.removeAllEvents();
    var idSelecionado = $('.select-profissional-ag option:selected').val();
    var status = $('#status option:selected').val();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type:"POST",
      url:'load-events',
      success: function(data){
        $.each(data, function(index, value){
          if(idSelecionado == '' && value.status == status) {
            calendar.addEvent(value);
          }
          else if(value.profissional_id == idSelecionado && value.status == status){
            calendar.addEvent(value);
          }
        })
      }
    })
  });
  $('#status').change(function () {
    calendar.removeAllEvents();
    var idSelecionado = $('.select-profissional-ag option:selected').val();
    var status = $('#status option:selected').val();
    $.ajax({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      },
      type:"POST",
      url:'load-events',
      success: function(data){
        $.each(data, function(index, value){
          if(idSelecionado == '' && value.status == status) {
            calendar.addEvent(value);
          }
          else if(value.profissional_id == idSelecionado && value.status == status){
            calendar.addEvent(value);
          }
        })
      }
    })
  });
});