$('#modalComanda').on('show.bs.modal', function (event) {
    var a = $(event.relatedTarget)
    var cliente = a.data('cliente')
    var numero = a.data('numero')
    var data = a.data('dia')
    var valor_total = a.data('valor_total')
    var id = a.data('id')
    var atendimento_id = a.data('atendimento_id')

    var modal = $(this)
    modal.find('.modal-title').text('Finalizar Comanda #'+numero)

    $cliente = document.getElementsByName('cliente_id')
    modal.find($cliente).val(cliente)

    $numero = document.getElementsByName('numero')
    modal.find($numero).val(numero)

    $data = document.getElementsByName('data')
    modal.find($data).val(data)

    $valor_total = document.getElementsByName('valor_total')
    modal.find($valor_total).val(valor_total.toFixed(2).replace(".", ","))

    $valor_final = document.getElementsByName('valor_final')
    modal.find($valor_final).val(valor_total.toFixed(2).replace(".", ","))

    $id = document.getElementById('id')
    modal.find($id).val(id)

    $atendimento_id = document.getElementById('atendimento_id')
    modal.find($atendimento_id).val(atendimento_id)

    $('.valor').val('0,00');
    $('.valor').blur();
})