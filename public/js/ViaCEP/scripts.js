/*
Quando o usuário selecionar a UF, o select de cidades puxará somente
as cidades daquela UF
*/

$(document).ready(function () {

    $.getJSON('/json/cidadesPorUF.json', function (data) {

        var items = [];
        var options = '<option selected disabled value="">UF</option>';

        $.each(data, function (key, val) {
            options += '<option value="' + val.sigla + '">' + val.sigla + '</option>';
        });
        $(".create .uf").html(options);
        $(".edit .uf").html(options);
        $(".view .uf").html(options);

        $(".create .uf").change(function () {

            var options_cidades = '';
            var str = "";

            $(".create .uf option:selected").each(function () {
                str += $(this).text();
            });

            $.each(data, function (key, val) {
                if (val.sigla == str) {
                    $.each(val.cidades, function (key_city, val_city) {
                        options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                    });
                }
            });


            $(".create .cidade").html(options_cidades);

        }).change();

        $(".edit .uf").change(function () {

            var options_cidades = '';
            var str = "";

            $(".edit .uf option:selected").each(function () {
                str += $(this).text();
            });

            $.each(data, function (key, val) {
                if (val.sigla == str) {
                    $.each(val.cidades, function (key_city, val_city) {
                        options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                    });
                }
            });


            $(".edit .cidade").html(options_cidades);

        }).change();

        $.getJSON('/json/cidades.json', function (data) {

            var options_cidades = '<option selected disabled value=""></option>';
    
            $.each(data, function (key, val) {
                $.each(val.cidades, function (key_city, val_city) {
                    options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                });
    
                $(".edit .cidade").html(options_cidades);
    
            });
        });

        $.getJSON('/json/cidades.json', function (data) {

            var options_cidades = '<option selected disabled value=""></option>';
    
            $.each(data, function (key, val) {
                $.each(val.cidades, function (key_city, val_city) {
                    options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                });
    
                $(".view .cidade").html(options_cidades);
    
            });
        });

    });

});

/*
Quando o usuário clicar no input CEP, o select de cidades puxará todas as cidades do brasil, 
para que o viaCEP consiga achar a cidade dentro do select
*/

function todasCidades() {
    $.getJSON('/json/cidades.json', function (data) {

        var options_cidades = '<option selected disabled value=""></option>';

        $.each(data, function (key, val) {
            $.each(val.cidades, function (key_city, val_city) {
                options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
            });

            $(".cidade").html(options_cidades);

        });
    });
}

/*
Depois de usuário ter preenchido o CEP, e passar para o campo UF, é executada a  primeira função
de puxar as cidades referente a UF, isso faz com que o campo de cidade se esvazie
e puxa somente as cidades daquela UF, porém não mantem a mesma cidade de antes (puxa a primeira cidade daquela uf).
Essa função mantém a mesma cidade.
*/

function mantemCidade() {
    var e = document.getElementById("cidade");
    var aux = e.options[e.selectedIndex].value;
    $.getJSON('/json/cidadesPorUF.json', function (data_2) {
        $(".cidade").empty();
        $(".uf").change(function () {

            var options_cidades = '';
            var str = "";

            $(".uf option:selected").each(function () {
                str += $(this).text();
            });

            $.each(data_2, function (key, val) {
                if (val.sigla == str) {
                    $.each(val.cidades, function (key_city, val_city) {
                        if (val_city === aux) {
                            options_cidades += '<option selected value="' + val_city + '">' + val_city + '</option>';
                        } else {
                            options_cidades += '<option value="' + val_city + '">' + val_city + '</option>';
                        }
                    });
                }
            });

            $(".cidade").html(options_cidades);

        }).change();
    });
}