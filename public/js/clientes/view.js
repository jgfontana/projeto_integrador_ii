$('#modalViewCliente').on('show.bs.modal', function (event) {
    var a = $(event.relatedTarget)
    var nome = a.data('nome')
    var nasc = a.data('nasc')
    var cpf = a.data('cpf')
    var sexo = a.data('sexo')
    var celular = a.data('celular')
    var fone = a.data('fone')
    var email = a.data('email')
    var alergia = a.data('alergia')
    var descricao = a.data('descricao')
    var cep = a.data('cep')
    var uf = a.data('uf')
    var cidade = a.data('cidade')
    var logradouro = a.data('logradouro')
    var numero = a.data('numero')
    var bairro = a.data('bairro')
    var complemento = a.data('complemento')

    var modal = $(this)
    modal.find('.modal-title').text('Visualizando cliente: ' + nome)

    $nome = document.getElementsByName('nome')
    modal.find($nome).val(nome)

    $nasc = document.getElementsByName('nasc')
    modal.find($nasc).val(nasc)

    $cpf = document.getElementsByName('cpf')
    modal.find($cpf).val(cpf)

    $sexo = document.getElementsByName('sexo')
    modal.find($sexo).val(sexo)

    $celular = document.getElementsByName('celular')
    modal.find($celular).val(celular)

    $fone = document.getElementsByName('fone')
    modal.find($fone).val(fone)

    $email = document.getElementsByName('email')
    modal.find($email).val(email)

    $alergia = document.getElementsByName('alergia')
    modal.find($alergia).val(alergia)

    $descricao = document.getElementsByName('descricao')
    modal.find($descricao).val(descricao)

    $cep = document.getElementsByName('cep')
    modal.find($cep).val(cep)

    $uf = document.getElementsByName('uf')
    modal.find($uf).val(uf)

    $cidade = document.getElementsByName('cidade')
    modal.find($cidade).val(cidade)

    $logradouro = document.getElementsByName('logradouro')
    modal.find($logradouro).val(logradouro)

    $numero = document.getElementsByName('numero')
    modal.find($numero).val(numero)

    $bairro = document.getElementsByName('bairro')
    modal.find($bairro).val(bairro)

    $complemento = document.getElementsByName('complemento')
    modal.find($complemento).val(complemento)

    if ($('.view .alergia').val() == '1') {
        $('.view .desc').show();
    } else {
        $('.view .desc').hide();
    }
    $('.view .alergia').change(function () {
        if ($('.view .alergia').val() == '1') {
            $('.view .desc').show();
            $(".view .descricao").attr("required", "req");
        } else {
            $('.view .desc').hide();
            $(".view .descricao").removeAttr('required')
        }
    });

    $('.view .form-control').prop('readonly', true);
    $('.view select').attr("disabled", true);

    $(".view .form-control").css("font-weight", "bold")

    $('.view .form-control').attr("placeholder", "");
})