$(document).ready(function () {
    $('#table_id').DataTable({
        language: {
            searchPlaceholder: "Pesquisar...",
            search: "<i class='fas fa-search icon_search_datatable'></i>",
            info: "_PAGE_ de _PAGES_ Página(s)",
            infoEmpty: "Não há entradas para mostrar",
            zeroRecords: "Nenhum registro correspondente encontrado",
            infoFiltered: "(filtrado do total de _MAX_ entradas)",
            lengthMenu: "Mostrando _MENU_ linhas",
            paginate: {
                first: "Primeiro",
                last: "Último",
                next: "Próximo",
                previous: "Anterior"
            },
        },
        order: [[1, "asc"]],
        info: false,
        pageLength: 10,
        lenghtMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Bfrtipl',
        buttons: [{
            text: 'Novo Cliente',
            action: function (e, dt, node, config) {
                $('#modalCliente').modal('show')
            },
            attr: {
                id: 'novo'
            }
        }],
        columnDefs: [
            { orderable: false, "targets": [2, 3, 5, 6, 7] },
            { orderable: true, "targets": [0, 1, 4] }
        ],

        initComplete: function () {
            $('.dataTables_filter input[type="search"]').css({ 'width': '250px', 'display': 'inline-block' });
        }
    });
});

