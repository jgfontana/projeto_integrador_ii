$(document).ready(function () {
    $('.cpf').mask('000.000.000-00', { reverse: true });
    $('.fone').mask('(00) 0000-0000');
    $('.cep').mask('00000-000');
    $('.preco').mask("#.##0,00", {reverse: true});
    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };

    $('.celular').mask(SPMaskBehavior, spOptions);
});