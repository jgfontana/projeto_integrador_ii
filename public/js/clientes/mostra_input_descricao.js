$(document).ready(function () {
    $('.create .desc').hide();
    $('.create .alergia').change(function () {
        if ($('.create .alergia').val() == '1') {
            $('.create .desc').show();
            $(".create .descricao").attr("required", "req");
        } else {
            $('.create .desc').hide();
            $(".create .descricao").removeAttr('required');
        }
    });
});