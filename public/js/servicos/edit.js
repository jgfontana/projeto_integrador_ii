$('#modalEditServico').on('show.bs.modal', function (event) {
    var a = $(event.relatedTarget)
    var id = a.data('id')
    var nome_servico = a.data('nome_servico')
    var duracao = a.data('duracao')
    var descricao = a.data('descricao')
    var preco = a.data('preco')
    var categoria_id = a.data('categoria_id')

    var modal = $(this)
    modal.find('.modal-title').text('Editando serviço: ' + nome_servico)

    $id = document.getElementsByName('id')
    modal.find($id).val(id)

    $nome_servico = document.getElementsByName('nome_servico')
    modal.find($nome_servico).val(nome_servico)

    $duracao = document.getElementsByName('duracao')
    modal.find($duracao).val(duracao)

    $descricao = document.getElementsByName('descricao')
    modal.find($descricao).val(descricao)

    $preco = document.getElementsByName('preco')
    modal.find($preco).val(preco)

    $categoria_id = document.getElementsByName('categoria_id')
    modal.find($categoria_id).val(categoria_id)
})