$('#modalViewServico').on('show.bs.modal', function (event) {
    var a = $(event.relatedTarget)
    var nome_servico = a.data('nome_servico')
    var duracao = a.data('duracao')
    var descricao = a.data('descricao')
    var preco = a.data('preco')
    var categoria_id = a.data('categoria_id')


    var modal = $(this)
    modal.find('.modal-title').text('Visualizando serviço: ' + nome_servico)

    $nome_servico = document.getElementsByName('nome_servico')
    modal.find($nome_servico).val(nome_servico)

    $duracao = document.getElementsByName('duracao')
    modal.find($duracao).val(duracao)

    $descricao = document.getElementsByName('descricao')
    modal.find($descricao).val(descricao)

    $preco = document.getElementsByName('preco')
    modal.find($preco).val(preco)


    $categoria_id = document.getElementsByName('categoria_id')
    modal.find($categoria_id).val(categoria_id)


    $('.view .form-control').prop('readonly', true);
    $('.view select').attr("disabled", true);

    $(".view .form-control").css("font-weight", "bold")

    $('.view .form-control').attr("placeholder", "");
})