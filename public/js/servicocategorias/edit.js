$('#modalEditCategoriaServico').on('show.bs.modal', function (event) {
    var a = $(event.relatedTarget)
    var id = a.data('id')
    var nome = a.data('nome')
    var descricao = a.data('descricao')

    var modal = $(this)
    modal.find('.modal-title').text('Editando categoria: ' + nome)

    $id = document.getElementsByName('id')
    modal.find($id).val(id)

    $nome = document.getElementsByName('nome')
    modal.find($nome).val(nome)

    $descricao = document.getElementsByName('descricao')
    modal.find($descricao).val(descricao)
})