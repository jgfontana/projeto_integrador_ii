$(document).ready(function () {
    $('#table_id').DataTable({
        language: {
            searchPlaceholder: "Pesquisar...",
            search: "<i class='fas fa-search icon_search_datatable'></i>",
            info: "_PAGE_ de _PAGES_ Página(s)",
            infoEmpty: "Não há entradas para mostrar",
            zeroRecords: "Nenhum registro correspondente encontrado",
            infoFiltered: "(filtrado do total de _MAX_ entradas)",
            lengthMenu: "Mostrando _MENU_ linhas",
            paginate: {
                first: "Primeiro",
                last: "Último",
                next: "Próximo",
                previous: "Anterior"
            },
        },
        order: [[1, "asc"]],
        info: false,
        pageLength: 10,
        lenghtMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
        dom: 'Bfrtipl',
        buttons: [{
            text: 'Nova Categoria',
            action: function (e, dt, node, config) {
                $('#modalCategoriaServico').modal('show');
            }
        }],
        columnDefs: [
            { orderable: false, "targets": [2, 3, 4] },
            { orderable: true, "targets": [0, 1] }
        ],

        initComplete: function () {
            $('.dataTables_filter input[type="search"]').css({ 'width': '250px', 'display': 'inline-block' });
        }
    });
});