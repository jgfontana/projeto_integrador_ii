<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CaixaSangria extends Model
{
    protected $table = "caixaSangrias"; 

    protected $fillable = [
        'valor_sangria', 
        'hora_sangria',
    ];

    public function caixa(){
        return $this->belongsTo(Caixa::class);
    }
}
