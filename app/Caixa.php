<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Caixa extends Model
{
    protected $table = "caixas"; 

    protected $fillable = [
        'valor_inicial', 
        'valor_final',
        'data', 
        'hora_abertura', 
        'hora_finalizacao', 
        'suprimento', 
        'hora_suprimento', 
        'observacao', 
        'status', 
        'caixaSangria_id',
    ];

    public function comandas(){
        return $this->hasMany(Comanda::class);
    }

    public function Sangrias(){
        return $this->hasMany(CaixaSangria::class);
    }
}
