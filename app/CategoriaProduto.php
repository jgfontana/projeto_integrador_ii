<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaProduto extends Model
{
    protected $table = "categoriaProdutos"; 

    protected $fillable = [
        'nome', 
        'descricao',
    ];

    public function produtos(){
        return $this->hasMany(Produto::class);
    }
}
