<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atendimento extends Model
{
    protected $table = "atendimentos"; 
    
    protected $fillable = [
        'data',  
        'observacao', 
        'status', 
        'cliente_id', 
        'comandagerada', 
    ];

    public function profissionalServicos()
    {
        return $this->belongsToMany(ProfissionalServico::class, 'atendimento_servico', 'profissionalServico_id', 'atendimento_id');
        //Classe, Nome da tabela Pivo (masterdetail), Nome do id da classe na tabela pivo, Nome do id desse model na tabela pivo 
    }

    public function cliente(){
        return $this->hasOne(Cliente::class);
    }

    public function comanda(){
        return $this->belongsTo(Comanda::class);
    }
}
