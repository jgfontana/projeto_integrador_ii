<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marca extends Model
{
    protected $table = "marcas";

    protected $fillable = [
        'nome', 
    ];

    public function linhas(){
        return $this->hasMany(Linha::class);
    }
}
