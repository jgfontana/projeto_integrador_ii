<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comanda extends Model
{
    protected $table = "comandas"; 

    protected $fillable = [
        'numero', 
        'data',
        'valor_total',
        'status',
        'atendimento_id',
        'cliente_id',
        'desconto',
        'valor_final',
        'troco',
        'recebido'
        //'profissional_id',
    ];

    public function profissional()
    {
        return $this->hasOne(Profissional::class);
    }

    public function atendimento(){
        return $this->hasOne(Atendimento::class);
    }

    public function comissao(){
        return $this->belongsTo(Comissao::class);
    }

    public function caixa(){
        return $this->belongsTo(Caixa::class);
    }

    public function formasPagamento()
    {
        return $this->belongsToMany(FormaPagamento::class, 'formaPagamento_comandas', 'formaPagamento_id', 'comanda_id');
        //Classe, Nome da tabela Pivo (masterdetail), Nome do id da classe na tabela pivo, Nome do id desse model na tabela pivo 
    }

    public function produtos()
    {
        return $this->belongsToMany(Produto::class, 'comanda_produto', 'produto_id', 'comanda_id');
        //Classe, Nome da tabela Pivo (masterdetail), Nome do id da classe na tabela pivo, Nome do id desse model na tabela pivo 
    }
}
