<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComandaProduto extends Model
{
    protected $table = "comanda_produto"; 

    protected $fillable = [
        'comanda_id',
        'produto_id',
    ];
}
