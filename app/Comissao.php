<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comissao extends Model
{
    protected $table = "comissoes"; 

    protected $fillable = [
        'data_inicial', 
        'data_final',
        'valor_total',
        'observacao',
        'comanda_id',
        'data_pagamento',
        'status',
        'ajusteComissao_id',
    ];

    public function comandas(){
        return $this->hasMany(Comanda::class);
    }

    public function ajustes(){
        return $this->hasMany(AjusteComissao::class);
    }

}
