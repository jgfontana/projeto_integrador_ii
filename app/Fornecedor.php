<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fornecedor extends Model
{
    protected $table = "fornecedores"; 

    protected $fillable = [
        'cnpj', 
        'inscr_estadual',
        'razao_social',
        'nome_fantasia',
        'email',
        'fone',
        'celular',
        'contato',
        'site',
        'observacao',
        'numero',
        'bairro',
        'cidade',
        'uf',
        'cep',
        'complemento',
    ];

    public function produtos(){
        return $this->hasMany(Produto::class);
    }
}
