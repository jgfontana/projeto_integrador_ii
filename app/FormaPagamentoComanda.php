<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaPagamentoComanda extends Model
{
    protected $table = "formaPagamento_comandas"; 

    protected $fillable = [
        'comanda_id', 
        'formaPagamento_id',
        'valor'
    ];
}
