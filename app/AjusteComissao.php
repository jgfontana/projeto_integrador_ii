<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AjusteComissao extends Model
{
    protected $table = "ajusteComissoes"; 

    protected $fillable = [
        'valor_ajuste', 
        'motivo',
    ];

    public function comissao(){
        return $this->belongsToMany(Comissao::class);
    }
}
