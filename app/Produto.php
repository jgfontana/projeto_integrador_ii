<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = "produtos"; 

    protected $fillable = [
        'nome', 
        'descricao',
        'venda',
        'consumo',
        'preco_custo',
        'preco_venda',
        'comissao',
        'estoque',
        'categoriaProduto_id',
        'fornecedor_id',
        'marca_id',
    ];

    public function comandas()
    {
        return $this->belongsToMany(Comanda::class, 'comanda_produto', 'comanda_id', 'produto_id');
        //Classe, Nome da tabela Pivo (masterdetail), Nome do id da classe na tabela pivo, Nome do id desse model na tabela pivo 
    }

    public function categoriaProduto(){
        return $this->belongsTo(CategoriaProduto::class);
    }

    public function fornecedor(){
        return $this->belongsTo(Fornecedor::class);
    }
}

