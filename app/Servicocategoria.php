<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Servicocategoria extends Model
{
    protected $table = "servicocategorias"; 

    protected $fillable = [
        'nome', 
        'descricao',
    ];

    public function servicos(){
        return $this->hasMany(Servico::class);
    }
}
