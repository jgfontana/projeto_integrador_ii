<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Pessoa extends Model
{
    protected $table = "pessoas";

    protected $fillable = [
        'nome',
        'sexo',
        'cpf',
        'nasc',
        'celular',
        'fone',
        'email',
        'observacao',
        'logradouro',
        'numero',
        'bairro',
        'cidade',
        'uf',
        'cep',
        'complemento',
    ];

    public function pessoable()
    {
        return $this->morphTo();
    }
}
