<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfissionalServico extends Model
{
    protected $table = "profissional_servico"; 

    protected $fillable = [
        'servico_id', 
        'profissional_id',
        'status',
    ];

    public function atendimentos()
    {
        return $this->belongsToMany(Atendimento::class, 'atendimento_servico', 'atendimento_id', 'profissionalServico_id');
        //Classe, Nome da tabela Pivo (masterdetail), Nome do id da classe na tabela pivo, Nome do id desse model na tabela pivo 
    }
}
