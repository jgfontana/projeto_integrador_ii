<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AtendimentoServico extends Model
{
    protected $table = "atendimento_servico"; 

    protected $fillable = [
        'profissionalServico_id',
        'atendimento_id',
        'hora_inicio',
        'hora_fim', 
        'duracao',
        'status',
    ];
}
