<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Servico extends Model
{
    protected $table = "servicos";

    protected $fillable = [
        'nome_servico',
        'duracao',
        'descricao',
        'preco',
        'categoria_id',
        'status',
    ];

    public function categoria()
    {
        return $this->belongsTo(Servicocategoria::class);
    }

    public function profissionais()
    {
        return $this->belongsToMany(Profissional::class, 'profissional_servico', 'profissional_id', 'servico_id');
        //Classe, Nome da tabela Pivo (masterdetail), Nome do id da classe na tabela pivo, Nome do id desse model na tabela pivo 
    }

    public function getPrecoAttribute()
    {
        $preco = $this->attributes['preco'];
        $format = number_format($preco, 2, ",", ".");

        return $format;
    }

    public function getDuracaoAttribute()
    {
        $aux = $this->attributes['duracao'];
        $duracao = Carbon::parse($aux)->format('H:i');

        return $duracao;
    }
}
