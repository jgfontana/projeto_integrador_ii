<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profissional extends Model
{
    protected $table = "profissionais"; 

    protected $fillable = [
        'cor', 
        'status', 
    ];

    public function pessoa(){
        return $this->morphOne(Pessoa::class, 'pessoable');
    }

    public function comanda()
    {
        return $this->belongsTo(Comanda::class);
    }

    public function servicos()
    {
        return $this->belongsToMany(Servico::class, 'profissional_servico', 'servico_id', 'profissional_id');
        //Classe, Nome da tabela Pivo (masterdetail), Nome do id da classe na tabela pivo, Nome do id desse model na tabela pivo 
    }
}
