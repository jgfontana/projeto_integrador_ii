<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linha extends Model
{
    protected $table = "linhas";

    protected $fillable = [
        'nome', 
        'marca_id',
    ];

    public function marca(){
        return $this->belongsTo(Marca::class);
    }

}
