<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormaPagamento extends Model
{
    protected $table = "formaPagamentos"; 

    protected $fillable = [
        'nome', 
        'status',
    ];

    public function comandas()
    {
        return $this->belongsToMany(Comanda::class, 'formaPagamento_comandas', 'comanda_id', 'formaPagamento_id');
        //Classe, Nome da tabela Pivo (masterdetail), Nome do id da classe na tabela pivo, Nome do id desse model na tabela pivo 
    }
}
