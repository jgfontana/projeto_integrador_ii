<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $table = "clientes"; 

    protected $fillable = [
        'alergia', 
        'descricao',
        'status',
    ];
    
    public function pessoa(){
        return $this->morphOne(Pessoa::class, 'pessoable');
    }

    public function atendimento(){
        return $this->belongsTo(Atendimento::class);
    }
}
