<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServicocategoriaRequest;
use App\Servicocategoria;
use Illuminate\Http\Request;

class ServicocategoriasController extends Controller
{
    public function index()
    {
        $categoriaServicos = Servicocategoria::all();
        return view('categoriaServicos.index', ['categoriaServicos' => $categoriaServicos]);
    }

    public function store(ServicocategoriaRequest $request)
    {
        $cat = $request->all();
        Servicocategoria::create($cat);
        return redirect()->route('categorias-servico');
    }

    public function destroy($id)
    {
        try {
            Servicocategoria::find($id)->delete();
            $ret = array('status' => 'ok', 'msg' => "null");
        } catch (\Illuminate\Database\QueryException $e) {
            $ret = array('status' => 'erro', 'msg' => $e->getMessage());
        } catch (\PDOException $e) {
            $ret = array('status' => 'erro', 'msg' => $e->getMessage());
        }
        return $ret;
    }

    public function update(ServicocategoriaRequest $request)
    {
        Servicocategoria::find($request->get('id'))->update([
            'nome'      => $request->get('nome'),
            'descricao' => $request->get('descricao')
        ]);
        
        return redirect()->route('categorias-servico');
    }
}
