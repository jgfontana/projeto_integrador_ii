<?php

namespace App\Http\Controllers;

use App\Atendimento;
use App\AtendimentoServico;
use App\Http\Requests\ProfissionalServicoRequest;
use App\Profissional;
use App\ProfissionalServico;
use App\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfissionalServicoController extends Controller
{
    public function index()
    {
        $profissionais = DB::table('pessoas')
            ->join('profissionais', 'pessoas.pessoable_id', '=', 'profissionais.id')
            ->where('pessoas.pessoable_type', 'App\Profissional')
            ->where('profissionais.status', '=', 'A')
            ->orderBy('pessoas.nome')
            ->select('pessoas.nome as nome', 'profissionais.id as id')
            ->get();

        $count = $profissionais->count();

        $servicos = DB::table('servicos')
            ->join('servicocategorias', 'servicocategorias.id', '=', 'servicos.categoria_id')
            ->select(
                'servicocategorias.nome as nome_categoria',
                'servicos.nome_servico as nome',
                'servicocategorias.id as id_categoria',
                'servicos.id as id',
                'servicos.preco as preco',
                'servicos.duracao as duracao',
                DB::raw('MAX(servicocategorias.id) as maximo')
            )
            ->where('servicos.status', '=', 'A')
            ->groupBy('servicos.id')
            ->orderBy('servicos.nome_servico')
            ->get();

        return view('profissional-servico.index', ['profissionais' => $profissionais, 'count' => $count, 'servicos' => $servicos]);
    }

    public function action(Request $request)
    {
        if ($request->ajax()) {
            $output = '';
            $aux = $request->get('query');
            $profissional = $request->get('profissional');
            if ($aux != '') {
                $data = DB::table('servicocategorias')
                    ->join('servicos', 'servicocategorias.id', '=', 'servicos.categoria_id')
                    ->join('profissional_servico', 'servicos.id', '=', 'profissional_servico.servico_id')
                    ->join('profissionais', 'profissional_servico.profissional_id', '=', 'profissionais.id')
                    ->where('profissionais.id', '=', $profissional)
                    ->where('profissional_servico.status', '=', 'A')
                    ->where('profissionais.status', '=', 'A')
                    ->where('servicos.status', '=', 'A')
                    ->select('servicos.nome_servico as nome', 'servicocategorias.nome as categoria', 'profissional_servico.id as id')
                    ->get();
            }

            foreach ($data as $row) {
                $output .=
                    '<tr>' .
                    '<td>' . $row->nome . '</td>' .
                    '<td>' . $row->categoria . '</td>' .
                    '<td style="text-align: center"><a onclick="return confirmaExclusao(' . $row->id . ')"><i style="color: darkred; font-size: 20px" class="fas fa-times"></i></a></td>' .
                    '</tr>';
            }

            $data = array(
                'table_data'  => $output,
            );

            echo json_encode($data);
        }
    }

    public function store(ProfissionalServicoRequest $request)
    {
        $profissional = $request->get('profissional_id');
        $servico = $request->get('servico_id');
        $ps = $request->all();

        $inativo = ProfissionalServico::where('profissional_id', '=', $profissional)
        ->where('servico_id', '=', $servico)
        ->where('status', '=', 'I')
        ->count();

        $findServico = DB::table('servicos')
            ->join('profissional_servico', 'servicos.id', '=', 'profissional_servico.servico_id')
            ->join('profissionais', 'profissional_servico.profissional_id', '=', 'profissionais.id')
            ->where('profissional_servico.profissional_id', '=', $profissional)
            ->where('profissional_servico.status', '=', 'A')
            ->select('profissional_servico.servico_id as servico')
            ->get();

        $i = 0;

        foreach ($findServico as $fs) {
            if ($fs->servico == $servico) {
                $i = 1;
            }
        }

        if ($i == 1) {
            return redirect()->back()->with('alert1', 'Profissional já exerce este serviço!')->with('prof', $profissional);
        } elseif ($inativo > 0) {
            ProfissionalServico::where('profissional_id', '=', $profissional)
            ->where('servico_id', '=', $servico)
            ->where('status', '=', 'I')
            ->update(['status' => 'A']);
            return redirect()->route('profissional-servico')->with('alert2', 'Serviço adicionado!')->with('prof', $profissional);
        } else {
            ProfissionalServico::create($ps);
            return redirect()->route('profissional-servico')->with('alert2', 'Serviço adicionado!')->with('prof', $profissional);
        }
    }

    public function destroy($id)
    {
        $existeAtendimento = AtendimentoServico::where('profissionalServico_id', '=', $id)->count();
        $existeAtendimentoAtivo = DB::table('atendimentos')
        ->join('atendimento_servico', 'atendimentos.id', '=', 'atendimento_servico.atendimento_id')
        ->join('profissional_servico', 'atendimento_servico.profissionalServico_id', '=', 'profissional_servico.id')
        ->where('atendimentos.status', '=', 'A')
        ->where('profissionalServico_id', '=', $id)
        ->count();

        if($existeAtendimentoAtivo > 0){
            $ret = array('status' => 'e', 'msg' => "null");
        }
        else if ($existeAtendimento > 0) {
            ProfissionalServico::where('id', '=', $id)->update(['status' => 'I']);
            $ret = array('status' => 'ok', 'msg' => "null");
        } else {
            try {
                ProfissionalServico::find($id)->delete();
                $ret = array('status' => 'ok', 'msg' => "null");
            } catch (\Illuminate\Database\QueryException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            } catch (\PDOException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            }
        }

        return $ret;
    }
}
