<?php

namespace App\Http\Controllers;

use App\Atendimento;
use App\Comanda;
use App\FormaPagamento;
use App\FormaPagamentoComanda;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class ComandasController extends Controller
{
    public function index()
    {
        $atendimentos = DB::table('atendimento_servico')
            ->join('atendimentos', 'atendimentos.id', '=', 'atendimento_servico.atendimento_id')
            ->join('clientes', 'clientes.id', '=', 'atendimentos.cliente_id')
            ->join('pessoas', 'clientes.id', '=', 'pessoas.pessoable_id')
            ->where('pessoas.pessoable_type', '=', 'App\Cliente')
            ->where('atendimentos.comandagerada', '=', 'N')
            ->select(
                'atendimentos.data as data',
                'pessoas.nome as cliente',
                'atendimentos.status as status',
                'atendimentos.id as id'
            )
            ->groupBy('pessoas.id', 'atendimentos.id')
            ->get();

        return view('comandas.index', ['atendimentos' => $atendimentos]);
    }

    public function index2()
    {
        $atendimentos = DB::table('servicos')
            ->join('profissional_servico', 'servicos.id', '=', 'profissional_servico.servico_id')
            ->join('atendimento_servico', 'atendimento_servico.profissionalServico_id', '=', 'profissional_servico.id')
            ->join('atendimentos', 'atendimento_servico.atendimento_id', '=', 'atendimentos.id')
            ->join('comandas', 'comandas.atendimento_id', '=', 'atendimentos.id')
            ->join('clientes', 'atendimentos.cliente_id', '=', 'clientes.id')
            ->join('pessoas', 'pessoas.pessoable_id', '=', 'clientes.id')
            ->where('pessoas.pessoable_type', '=', 'App\Cliente')
            ->where('comandas.status', '=', 'A')
            ->select(
                DB::raw('sum(servicos.preco) as total'),
                'pessoas.nome as cliente',
                'comandas.numero as numero',
                'comandas.data as data',
                'comandas.id as id',
                'atendimentos.id as atendimento_id'
            )
            ->groupBy('pessoas.id', 'comandas.id')
            ->get();

        $formas = FormaPagamento::orderBy('nome')->get();


        return view('comandas.index2', ['atendimentos' => $atendimentos, 'formas' => $formas]);
    }

    public function index3()
    {
        $atendimentos = DB::table('comandas')
            ->join('clientes', 'comandas.cliente_id', '=', 'clientes.id')
            ->join('pessoas', 'pessoas.pessoable_id', '=', 'clientes.id')
            ->where('pessoas.pessoable_type', '=', 'App\Cliente')
            ->where('comandas.status', '=', 'F')
            ->select(
                'pessoas.nome as cliente',
                'comandas.numero as numero',
                'comandas.data as data',
                'comandas.id as id',
                'comandas.desconto as desconto',
                'comandas.valor_total as total',
                'comandas.valor_final as final',
                'comandas.recebido as recebido',
                'comandas.troco as troco'
            )
            ->groupBy('pessoas.id', 'comandas.id')
            ->get();

        $formas = FormaPagamento::orderBy('nome')->get();


        return view('comandas.index3', ['atendimentos' => $atendimentos, 'formas' => $formas]);
    }

    public function geraComanda(Request $request)
    {
        if ($request->ajax()) {
            try {
                Atendimento::find($request->id)->update([
                    'comandagerada' => 'S'
                ]);

                $ultimoN = 0;
                $ultimoN = Comanda::orderBy('numero', 'desc')->pluck('numero')->first();
                $n = $ultimoN + 1;


                $dados = DB::table('servicos')
                    ->join('profissional_servico', 'servicos.id', '=', 'profissional_servico.servico_id')
                    ->join('atendimento_servico', 'atendimento_servico.profissionalServico_id', '=', 'profissional_servico.id')
                    ->join('atendimentos', 'atendimento_servico.atendimento_id', '=', 'atendimentos.id')
                    ->join('clientes', 'atendimentos.cliente_id', '=', 'clientes.id')
                    ->join('pessoas', 'pessoas.pessoable_id', '=', 'clientes.id')
                    ->where('atendimentos.id', '=', $request->id)
                    ->select(
                        DB::raw('sum(servicos.preco) as valor_total'),
                        'clientes.id as cliente_id'
                    )
                    ->groupBy('clientes.id', 'atendimentos.id')
                    ->get();

                foreach ($dados as $d) {
                    $valor_total = $d->valor_total;
                    $cliente_id = $d->cliente_id;
                }


                $data = Carbon::now()->format('Y-m-d');

                Comanda::create([
                    'numero'            => $n,
                    'data'              => $data,
                    'valor_total'       => $valor_total,
                    'status'            => 'A',
                    'atendimento_id'    => $request->id,
                    'cliente_id'        => $cliente_id,
                ]);

                $ret = array('status' => 'ok', 'msg' => "null");
            } catch (\Illuminate\Database\QueryException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            } catch (\PDOException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            }

            return $ret;
        }
    }

    public function mostraServicos(Request $request)
    {
        if ($request->ajax()) {
            $servicos = DB::table('servicos')
            ->join('profissional_servico', 'servicos.id', '=', 'profissional_servico.servico_id')
            ->join('atendimento_servico', 'atendimento_servico.profissionalServico_id', '=', 'profissional_servico.id')
            ->join('atendimentos', 'atendimento_servico.atendimento_id', '=', 'atendimentos.id')
            ->where('atendimentos.id', '=', $request->atendimento_id)
            ->select(
                'servicos.nome_servico as servico',
                'servicos.preco as preco'
            )
            ->get();

            return response()->json($servicos);
        }
    }

    public function update(Request $request)
    {
        $comanda_id = $request->get('id');
        $desconto = $request->get('desconto');

        $valor_final_aux = str_replace(",", ".", $request->get('valor_final'));
        $valor_final = preg_replace('/\.(?=.*\.)/', '', $valor_final_aux);

        $troco_aux = str_replace(",", ".", $request->get('troco'));
        $troco = preg_replace('/\.(?=.*\.)/', '', $troco_aux);

        $recebido_aux = str_replace(",", ".", $request->get('recebido'));
        $recebido = preg_replace('/\.(?=.*\.)/', '', $recebido_aux);

        Comanda::find($comanda_id)->update([
            'status'        => 'F',
            'desconto'      => $desconto,
            'valor_final'   => $valor_final,
            'troco'         => $troco,
            'recebido'      => $recebido
        ]);

        $array_formaPagamento_id = request()->formaPagamento_id;
        $array = request()->valor;
        $array_aux = array();
        foreach ($array as $index => $item) {
            unset($array_aux);
            $array_aux = str_replace(",", ".", $item);
            $array_valor[] = preg_replace('/\.(?=.*\.)/', '', $array_aux);
        }

        foreach($array_formaPagamento_id as $index => $formaPagamento_id) {
            FormaPagamentoComanda::create([
                'formaPagamento_id' => $formaPagamento_id,
                'valor'             => $array_valor[$index],
                'comanda_id'        => $comanda_id
            ]);
        }

        return redirect()->route('comandas.index3');
    }

    public function geraRecibo($id){

        $cliente = DB::table('pessoas')
        ->join('clientes', 'clientes.id', '=', 'pessoas.pessoable_id')
        ->join('comandas', 'comandas.cliente_id', '=', 'clientes.id')
        ->where('pessoas.pessoable_type', '=', 'App\Cliente')
        ->where('comandas.id', '=', $id)
        ->select('pessoas.nome as nome')
        ->get();

        $comanda = Comanda::where('id', '=', $id)->get();

        $atendimento = DB::table('atendimentos')
        ->join('comandas', 'comandas.atendimento_id', '=', 'atendimentos.id')
        ->where('comandas.id', '=', $id)
        ->select('atendimentos.id as id')
        ->get();

        foreach ($atendimento as $item){
            $atendimento_id = $item->id;
        }

        $servicos = DB::table('servicos')
            ->join('profissional_servico', 'servicos.id', '=', 'profissional_servico.servico_id')
            ->join('atendimento_servico', 'atendimento_servico.profissionalServico_id', '=', 'profissional_servico.id')
            ->join('atendimentos', 'atendimento_servico.atendimento_id', '=', 'atendimentos.id')
            ->where('atendimentos.id', '=', $atendimento_id)
            ->select(
                'servicos.nome_servico as servico',
                'servicos.preco as preco'
            )
            ->get();

        $formas = DB::table('comandas')
        ->join('formaPagamento_comandas', 'comandas.id', '=', 'formaPagamento_comandas.comanda_id')
        ->join('formaPagamentos', 'formaPagamento_comandas.formaPagamento_id', '=', 'formaPagamentos.id')
        ->where('comandas.id', '=', $id)
        ->select('formaPagamentos.nome as forma', 'formaPagamento_comandas.valor as valor')
        ->get();

        $view = \view('comandas.recibo', ['cliente' => $cliente, 'comanda' => $comanda, 'servicos' => $servicos, 'formas' => $formas]);
        $html = $view->render();
        $pdf = \PDF::loadHTML($html)->setPaper('a4', 'portrait')->setWarnings(false)->save('recibo.pdf');

        return $pdf->stream('relatorio.pdf');
    }
}
