<?php

namespace App\Http\Controllers;

use App\Http\Requests\ServicoRequest;
use App\ProfissionalServico;
use App\Servico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servico::where('status', '=', 'A')->get();
        return view('servicos.index', ['servicos' => $servicos]);
    }

    public function store(ServicoRequest $request)
    {
        $aux = str_replace(",", ".", $request->get('preco'));
        $preco = preg_replace('/\.(?=.*\.)/', '', $aux);
        /* Ex: Se o valor for 1.234,56 , o formato será 1234.56 (mysql nao aceita valor com virgula) */

        Servico::create([
            'nome_servico'  => $request->get('nome_servico'),
            'duracao'       => $request->get('duracao'),
            'descricao'     => $request->get('descricao'),
            'preco'         => $preco,
            'categoria_id'  => $request->get('categoria_id'),
            'status'        => 'A'
        ]);

        return redirect()->route('servicos');
    }

    public function destroy($id)
    {
        $profissionalExerce = ProfissionalServico::where('servico_id', '=', $id)->count();

        $existeAtendimentoAtivo = DB::table('atendimentos')
        ->join('atendimento_servico', 'atendimentos.id', '=', 'atendimento_servico.atendimento_id')
        ->join('profissional_servico', 'atendimento_servico.profissionalServico_id', '=', 'profissional_servico.id')
        ->join('servicos', 'profissional_servico.servico_id', '=', 'servicos.id')
        ->where('atendimentos.status', '=', 'A')
        ->where('servicos.id', '=', $id)
        ->count();

        if($existeAtendimentoAtivo > 0){
            $ret = array('status' => 'e2', 'msg' => "null");
        }
        else if ($profissionalExerce > 0) {
            Servico::where('id', '=', $id)->update(['status' => 'I']);
            $ret = array('status' => 'ok', 'msg' => "null");
        } else {
            try {
                Servico::find($id)->delete();
                $ret = array('status' => 'ok', 'msg' => "null");
            } catch (\Illuminate\Database\QueryException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            } catch (\PDOException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            }
        }
        return $ret;
    }

    public function update(ServicoRequest $request)
    {
        $aux = str_replace(",", ".", $request->get('preco'));
        $preco = preg_replace('/\.(?=.*\.)/', '', $aux);

        Servico::find($request->get('id'))->update([
            'nome_servico'  => $request->get('nome_servico'),
            'duracao'       => $request->get('duracao'),
            'descricao'     => $request->get('descricao'),
            'preco'         => $preco,
            'categoria_id'  => $request->get('categoria_id')
        ]);

        return redirect()->route('servicos');
    }
}
