<?php

namespace App\Http\Controllers;

use App\Atendimento;
use App\Cliente;
use App\Http\Requests\ClienteRequest;
use App\Http\Requests\PessoaRequest;
use App\Pessoa;
use Illuminate\Http\Request;

class ClientesController extends Controller
{
    public function index()
    {
        $clientes = Cliente::where('status', '=', 'A')->get();
        return view('clientes.index', ['clientes' => $clientes]);
    }

    public function store(ClienteRequest $request_c, PessoaRequest $request_p)
    {
        $cliente = Cliente::create([
            'alergia'       => $request_c->get('alergia'),
            'descricao'     => $request_c->get('descricao'),
            'status'        => 'A'
        ]);

        $cliente->pessoa()->create([
            'nome'          => $request_p->get('nome'),
            'sexo'          => $request_p->get('sexo'),
            'cpf'           => $request_p->get('cpf'),
            'nasc'          => $request_p->get('nasc'),
            'celular'       => $request_p->get('celular'),
            'fone'          => $request_p->get('fone'),
            'email'         => $request_p->get('email'),
            'observacao'    => $request_p->get('observacao'),
            'logradouro'    => $request_p->get('logradouro'),
            'numero'        => $request_p->get('numero'),
            'bairro'        => $request_p->get('bairro'),
            'cidade'        => $request_p->get('cidade'),
            'uf'            => $request_p->get('uf'),
            'cep'           => $request_p->get('cep'),
            'complemento'   => $request_p->get('complemento')
        ]);

        return redirect()->route('clientes');
    }

    public function storeFast(PessoaRequest $request_p)
    {
        $cliente = Cliente::create([
            'alergia'       => false,
            'status'        => 'A'
        ]);

        $cliente->pessoa()->create([
            'nome'          => $request_p->get('nome-cliente'),
            'sexo'          => $request_p->get('sexo'),
        ]);

        return redirect()->back()->with('ok', $cliente->id);
    }

    public function destroy($id)
    {
        $existeAtendimento = Atendimento::where('cliente_id', '=', $id)->count();

        if ($existeAtendimento > 0) {
            Cliente::where('id', '=', $id)->update(['status' => 'I']);
            $ret = array('status' => 'ok', 'msg' => "null");
        } else {
            try {
                $cliente = Cliente::find($id);
                $cliente->pessoa->delete();
                $cliente->delete();
                $ret = array('status' => 'ok', 'msg' => "null");
            } catch (\Illuminate\Database\QueryException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            } catch (\PDOException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            }
        }
        return $ret;
    }

    public function update(ClienteRequest $request_c, PessoaRequest $request_p)
    {
        $cliente = Cliente::find($request_c->get('id'));

        $cliente->update([
            'alergia'       => $request_c->get('alergia'),
            'descricao'     => $request_c->get('descricao')
        ]);

        $cliente->pessoa->update([
            'nome'          => $request_p->get('nome'),
            'sexo'          => $request_p->get('sexo'),
            'cpf'           => $request_p->get('cpf'),
            'nasc'          => $request_p->get('nasc'),
            'celular'       => $request_p->get('celular'),
            'fone'          => $request_p->get('fone'),
            'email'         => $request_p->get('email'),
            'observacao'    => $request_p->get('observacao'),
            'logradouro'    => $request_p->get('logradouro'),
            'numero'        => $request_p->get('numero'),
            'bairro'        => $request_p->get('bairro'),
            'cidade'        => $request_p->get('cidade'),
            'uf'            => $request_p->get('uf'),
            'cep'           => $request_p->get('cep'),
            'complemento'   => $request_p->get('complemento')
        ]);

        return redirect()->route('clientes');
    }
}
