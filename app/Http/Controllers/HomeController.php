<?php

namespace App\Http\Controllers;

use App\Atendimento;
use App\Cliente;
use App\Comanda;
use App\Pessoa;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // Valor do faturamento do dia dashboard
        $faturamento = DB::table('comandas')
            ->select(
                DB::raw('sum(valor_final) as total')
            )
            ->where('data', '=', DB::raw('curdate()'))
            ->where('status', '=', 'F')
            ->get();

        // Número de comandas fechadas no dia dashboard
        $comandas = Comanda::where('data', '=', DB::raw('curdate()'))
            ->where('status', '=', 'F')
            ->count();

        // Clientes novos abertos no dia dashboard
        $clientes = Cliente::whereDate('created_at', '=', date('Y-m-d'))->count();

        // Atendimentos realizados no dia dashboard
        $atendimentos = Atendimento::where('data', '=', date('Y-m-d'))
            ->count();

        // Grafico Atendimentos finalizados por Profissional
        $at_finalizado = DB::table('atendimentos')
            ->join('atendimento_servico', 'atendimento_servico.atendimento_id', '=', 'atendimentos.id')
            ->join('profissional_servico', 'profissional_servico.id', '=', 'atendimento_servico.profissionalServico_id')
            ->join('profissionais', 'profissionais.id', '=', 'profissional_servico.profissional_id')
            ->join('pessoas', 'pessoas.pessoable_id', '=', 'profissionais.id')
            ->where('pessoas.pessoable_type', '=', 'App\Profissional')
            ->where('profissionais.status', '=', 'A')
            ->where('atendimentos.status', '=', 'F')
            ->select(
                DB::raw('count(atendimentos.id) as qtd'),
                'pessoas.nome as profissional',
                'profissionais.cor as cor'
            )
            ->groupBy('pessoas.id')
            ->get();
        foreach ($at_finalizado as $item) {
            $qtd_at_finalizado[] = $item->qtd;
            $nome_at_finalizado[] = $item->profissional;
            $cor_at_finalizado[] = $item->cor;
        }

        $temp_nome_at_finalizado = $nome_at_finalizado;
        $result_nome_at_finalizado = "'" . implode("', '", $temp_nome_at_finalizado) . "'";

        $temp_cor_at_finalizado = $cor_at_finalizado;
        $result_cor_at_finalizado = "'" . implode("', '", $temp_cor_at_finalizado) . "'";


        // Grafico Agendamentos em aberto por profissional
        $ag_aberto = DB::table('atendimentos')
            ->join('atendimento_servico', 'atendimento_servico.atendimento_id', '=', 'atendimentos.id')
            ->join('profissional_servico', 'profissional_servico.id', '=', 'atendimento_servico.profissionalServico_id')
            ->join('profissionais', 'profissionais.id', '=', 'profissional_servico.profissional_id')
            ->join('pessoas', 'pessoas.pessoable_id', '=', 'profissionais.id')
            ->where('pessoas.pessoable_type', '=', 'App\Profissional')
            ->where('atendimentos.status', '=', 'A')
            ->select(
                DB::raw('count(atendimentos.id) as qtd'),
                'pessoas.nome as profissional',
                'profissionais.cor as cor'
            )
            ->groupBy('pessoas.id')
            ->get();
        $nome_ag_aberto = [];
        $cor_ag_aberto = [];
        $qtd_ag_aberto = [];
        foreach ($ag_aberto as $item) {
            $qtd_ag_aberto[] = $item->qtd;
            $nome_ag_aberto[] = $item->profissional;
            $cor_ag_aberto[] = $item->cor;
        }

        $temp_nome_ag_aberto = $nome_ag_aberto;
        $result_nome_ag_aberto = "'" . implode("', '", $temp_nome_ag_aberto) . "'";

        $temp_cor_ag_aberto = $cor_ag_aberto;
        $result_cor_ag_aberto = "'" . implode("', '", $temp_cor_ag_aberto) . "'";

        // faturamento por dia

        $fat_dia = DB::table('comandas')
            ->select(
                DB::raw('DATE_FORMAT(data, "%d/%m/%Y") as data'),
                DB::raw('SUM(valor_final) as valor')
            )
            ->where('status', '=', 'F')

            ->groupBy(DB::raw('DATE_FORMAT(data, "%d/%m/%Y")'))
            ->get();
                
        foreach($fat_dia as $item){
            $dia[] = $item->data;
            $valor[] = $item->valor;
        }

        $temp_dia = $dia;
        $result_dia = "'" . implode("', '", $temp_dia) . "'";

        $temp_valor = $valor;
        $result_valor = "'" . implode("', '", $temp_valor) . "'";
        

        return view('home', [
            'faturamento' => $faturamento,
            'comandas' => $comandas,
            'clientes' => $clientes,
            'atendimentos' => $atendimentos,
            'qtd_at_finalizado' => $qtd_at_finalizado,
            'result_nome_at_finalizado' => $result_nome_at_finalizado,
            'result_cor_at_finalizado' => $result_cor_at_finalizado,
            'qtd_ag_aberto' => $qtd_ag_aberto,
            'result_nome_ag_aberto' => $result_nome_ag_aberto,
            'result_cor_ag_aberto' => $result_cor_ag_aberto,
            'result_dia' => $result_dia,
            'result_valor' => $result_valor
        ]);
    }
}
