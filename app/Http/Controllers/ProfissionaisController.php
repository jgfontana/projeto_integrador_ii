<?php

namespace App\Http\Controllers;

use App\Http\Requests\PessoaRequest;
use App\Http\Requests\ProfissionalRequest;
use App\Profissional;
use App\ProfissionalServico;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProfissionaisController extends Controller
{
    public function index()
    {
        $profissionais = Profissional::where('status', '=', 'A')->get();
        return view('profissionais.index', ['profissionais' => $profissionais]);
    }

    public function store(ProfissionalRequest $request_c, PessoaRequest $request_p)
    {
        $profissional = Profissional::create([
            'cor'           => $request_c->get('cor'),
            'status'        => 'A'
        ]);

        $profissional->pessoa()->create([
            'nome'          => $request_p->get('nome'),
            'sexo'          => $request_p->get('sexo'),
            'cpf'           => $request_p->get('cpf'),
            'nasc'          => $request_p->get('nasc'),
            'celular'       => $request_p->get('celular'),
            'fone'          => $request_p->get('fone'),
            'email'         => $request_p->get('email'),
            'observacao'    => $request_p->get('observacao'),
            'logradouro'    => $request_p->get('logradouro'),
            'numero'        => $request_p->get('numero'),
            'bairro'        => $request_p->get('bairro'),
            'cidade'        => $request_p->get('cidade'),
            'uf'            => $request_p->get('uf'),
            'cep'           => $request_p->get('cep'),
            'complemento'   => $request_p->get('complemento')
        ]);

        return redirect()->route('profissionais');
    }

    public function destroy($id)
    {
        $exerceServico = ProfissionalServico::where('profissional_id', '=', $id)->count();
        $existeAtendimentoAtivo = DB::table('atendimentos')
        ->join('atendimento_servico', 'atendimentos.id', '=', 'atendimento_servico.atendimento_id')
        ->join('profissional_servico', 'atendimento_servico.profissionalServico_id', '=', 'profissional_servico.id')
        ->join('profissionais', 'profissional_servico.profissional_id', '=', 'profissionais.id')
        ->where('atendimentos.status', '=', 'A')
        ->where('profissionais.id', '=', $id)
        ->count();

        if($existeAtendimentoAtivo > 0){
            $ret = array('status' => 'e3', 'msg' => "null");
        }
        else if ($exerceServico > 0) {
            Profissional::where('id', '=', $id)->update(['status' => 'I']);
            $ret = array('status' => 'ok', 'msg' => "null");
        } else {
            try {
                $profissional = Profissional::find($id);
                $profissional->pessoa->delete();
                $profissional->delete();
                $ret = array('status' => 'ok', 'msg' => "null");
            } catch (\Illuminate\Database\QueryException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            } catch (\PDOException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            }
        }
        return $ret;
    }

    public function update(ProfissionalRequest $request_c, PessoaRequest $request_p)
    {
        $profissional = Profissional::find($request_c->get('id'));

        $profissional->update([
            'cor'           => $request_c->get('cor')
        ]);

        $profissional->pessoa->update([
            'nome'          => $request_p->get('nome'),
            'sexo'          => $request_p->get('sexo'),
            'cpf'           => $request_p->get('cpf'),
            'nasc'          => $request_p->get('nasc'),
            'celular'       => $request_p->get('celular'),
            'fone'          => $request_p->get('fone'),
            'email'         => $request_p->get('email'),
            'observacao'    => $request_p->get('observacao'),
            'logradouro'    => $request_p->get('logradouro'),
            'numero'        => $request_p->get('numero'),
            'bairro'        => $request_p->get('bairro'),
            'cidade'        => $request_p->get('cidade'),
            'uf'            => $request_p->get('uf'),
            'cep'           => $request_p->get('cep'),
            'complemento'   => $request_p->get('complemento')
        ]);

        return redirect()->route('profissionais');
    }
}
