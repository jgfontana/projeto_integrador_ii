<?php

namespace App\Http\Controllers;

use App\Atendimento;
use App\AtendimentoServico;
use App\Cliente;
use App\Http\Requests\AgendamentoRequest;
use App\Http\Requests\AtendimentoServicoRequest;
use App\Profissional;
use App\ProfissionalServico;
use App\Servico;
use AtendimentoServicoSeeder;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;

class AgendamentosController extends Controller
{
    public function index()
    {
        $agendamentos = Atendimento::all();

        $clientes = Cliente::where('status', '=', 'A')->get();

        $profissionais = DB::table('pessoas')
            ->join('profissionais', 'pessoas.pessoable_id', '=', 'profissionais.id')
            ->where('pessoas.pessoable_type', 'App\Profissional')
            ->where('profissionais.status', '=', 'A')
            ->orderBy('pessoas.nome')
            ->select('pessoas.nome as nome', 'profissionais.id as id', 'profissionais.cor as cor')
            ->get();

        $count = $profissionais->count();

        $servicos = DB::table('servicocategorias')
            ->join('servicos', 'servicos.categoria_id', '=', 'servicocategorias.id')
            ->rightjoin('profissional_servico', 'servicos.id', '=', 'profissional_servico.servico_id')
            ->join('profissionais', 'profissional_servico.profissional_id', '=', 'profissionais.id')
            ->select(
                'servicocategorias.nome as nome_categoria',
                'servicos.nome_servico as nome',
                'servicocategorias.id as id_categoria',
                'servicos.id as id',
                DB::raw("FORMAT(servicos.preco,2,'de_DE') as preco"),
                DB::raw("TIME_FORMAT(servicos.duracao, '%H:%i') as duracao"),
                DB::raw('MAX(servicocategorias.id) as maximo')
            )
            ->where('servicos.status', '=', 'A')
            ->where('profissionais.status', '=', 'A')
            ->groupBy('servicos.id')
            ->orderBy('servicos.nome_servico')
            ->get();

        return view('agendamentos.index', ['agendamentos' => $agendamentos, 'clientes' => $clientes, 'servicos' => $servicos, 'profissionais' => $profissionais, 'count' => $count]);
    }

    public function loadEvents()
    {
        $events = DB::table('profissionais')
            ->join('profissional_servico', 'profissionais.id', '=', 'profissional_servico.profissional_id')
            ->join('atendimento_servico', 'profissional_servico.id', '=', 'atendimento_servico.profissionalServico_id')
            ->join('atendimentos', 'atendimento_servico.atendimento_id', '=', 'atendimentos.id')
            ->join('clientes', 'atendimentos.cliente_id', '=', 'clientes.id')
            ->join('pessoas', 'clientes.id', '=', 'pessoas.pessoable_id')
            ->where('pessoas.pessoable_type', '=', 'App\Cliente')
            ->select(
                DB::raw("DATE_FORMAT(atendimento_servico.hora_inicio, '%Y-%m-%d') as data2"),
                'clientes.id as cliente_id',
                'atendimentos.data as data',
                'profissional_servico.profissional_id as profissional_id',
                'profissional_servico.servico_id as servico_id',
                'atendimento_servico.hora_inicio as start',
                'atendimento_servico.hora_fim as end',
                'atendimento_servico.duracao as duracao',
                'atendimento_servico.id as atendimento_servico_id',
                'pessoas.nome as title',
                'atendimentos.id as id',
                'atendimentos.observacao as obs',
                'atendimento_servico.status as status',
                'profissionais.cor as color'
            )
            ->get();
        return response()->json($events);
    }

    public function getProfissionais(Request $request)
    {
        if ($request->ajax()) {
            $profissionais = DB::table('servicos')
                ->join('profissional_servico', 'servicos.id', '=', 'profissional_servico.servico_id')
                ->join('profissionais', 'profissional_servico.profissional_id', '=', 'profissionais.id')
                ->join('pessoas', 'profissionais.id', '=', 'pessoas.pessoable_id')
                ->where('servicos.id', '=', $request->servico_id)
                ->where('profissional_servico.status', '=', 'A')
                ->where('servicos.status', '=', 'A')
                ->where('profissionais.status', '=', 'A')
                ->select('profissionais.id as id', 'pessoas.nome as nome')
                ->get();

            foreach ($profissionais as $profissional) {
                $profissionaisArray[$profissional->id] = $profissional->nome;
            }

            return response()->json($profissionaisArray);
        }
    }

    public function getServicos(Request $request)
    {
        if ($request->ajax()) {
            $servicos = Servico::where('id', '=', $request->servico_id)->get();

            foreach ($servicos as $serv) {
                $duracaoArray[$serv->duracao] = $serv->preco;
            }

            return response()->json($duracaoArray);
        }
    }

    public function getPrecos(Request $request)
    {
        if ($request->ajax()) {
            $array = request()->get('servico_id');

            foreach ($array as $index => $servico_id) {
                $aux[] = DB::table('servicos')->select('preco')->where('id', '=', $servico_id)->pluck('preco');
            }

            return response()->json($aux);
        }
    }

    public function getAlerta(Request $request)
    {
        if ($request->ajax()) {
            $testa = Cliente::where('id', '=', $request->cliente_id)
                ->pluck('alergia');

            if ($testa == true) {
                $descricao = Cliente::where('id', '=', $request->cliente_id)
                    ->pluck('descricao');
                return response()->json($descricao);
            } else {
                $descricao = false;
                return response()->json($descricao);
            }
        }
    }

    public function store(AgendamentoRequest $request)
    {
        $array_inicio = request()->hora_inicio; //recebe a(s) hora(s) de ínicio do(s) serviço(s).
        $array_duracao = request()->duracao; //recebe a(s) duração(ões) do(s) serviço(s).
        $array_profissional_id = request()->profissional_id; // recebe o(s) profissional
        $array_servico_id = request()->servico_id; // recebe o(s) serviço(s);

        $data = $request->get('data'); //recebe a data do agendamento

        foreach ($array_inicio as $index => $init) { // percorrer todos os serviços adicionados
            $inicio[] = '' . $init . ':00'; //pega a hora de inicio (H:m:s)
            $duracao[] = '' . $array_duracao[$index] . ':00'; //pega a duracao do atendimento (H:m:s).
        }

        $x = 0;
        foreach ($inicio as $index => $time1) {
            $times = array($time1, $duracao[$index]); //recebe a hora de inicio e a duracao em um array
            $seconds = 0;
            //soma a hora de inicio + duracao
            foreach ($times as $time) {
                list($hour, $minute, $second) = explode(':', $time);
                $seconds += $hour * 3600;
                $seconds += $minute * 60;
                $seconds += $second;
            }
            $hours = floor($seconds / 3600);
            $seconds -= $hours * 3600;
            $minutes  = floor($seconds / 60);
            $seconds -= $minutes * 60;
            //fim da soma

            // se passar da meia noite, adiciona um dia a mais na data do fim e ajusta a hora (para nao passar de 23:59)
            if ($hours >= 24) {
                $hours = $hours - 24;
                $data_fim = new DateTime($data);
                $data_fim = $data_fim->format("Y-m-d");
                $data_fim = date('Y-m-d', strtotime('+1 days', strtotime($data_fim)));
                $x = 1;
            } else {
                if ($x == 1) {
                    $data = new DateTime($data);
                    $data = $data->format("Y-m-d");
                    $data = date('Y-m-d', strtotime('+1 days', strtotime($data)));
                    $data_fim = new DateTime($data);
                    $data_fim = $data_fim->format("Y-m-d");
                    $x = 0;
                } else {
                    $data_fim = new DateTime($data);
                    $data_fim = $data_fim->format("Y-m-d");
                }
            }

            //corrige formato se for de 0 a 9 (evitar que fique 1:0:0 por exemplo... o certo é 01:00:00)
            if ($hours < 10) {
                $hours = '0' . $hours;
            }
            if ($minutes < 10) {
                $minutes = '0' . $minutes;
            }
            if ($seconds < 10) {
                $seconds = '0' . $seconds;
            }

            $resultado[] = '' . $hours . ':' . $minutes . ':' . $seconds; // resultado da soma (inicio + duracao) no formato 00:00:00 (H:m:s)

            $hora_inicio[] = '' . $data . ' ' . $inicio[$index]; //Deixar no formato "Y-m-d H:i:s" (padrão do fullcalendar)"
            $hora_fim[] = '' . $data_fim . ' ' . $resultado[$index];
            $dur[] = '' . $duracao[$index];
        }


        $agendamento = Atendimento::create([
            'data'          => $request->get('data'),
            'observacao'    => $request->get('observacao'),
            'cliente_id'    => $request->get('cliente_id'),
            'status'        => 'A',
            'comandagerada' => 'N'
        ]);

        $atendimento_id = $agendamento->id;

        foreach ($array_profissional_id as $index => $id) {
            $array_profissional_servico_id[] = DB::table('profissional_servico')
                ->where('profissional_id', '=', $id)
                ->where('servico_id', '=', $array_servico_id[$index])
                ->select('profissional_servico.id as id')
                ->value('id');
        }

        foreach ($array_profissional_servico_id as $index => $profissionalServico_id) {
            AtendimentoServico::create([
                'profissionalServico_id'    => $profissionalServico_id,
                'atendimento_id'            => $atendimento_id,
                'hora_inicio'               => $hora_inicio[$index],
                'hora_fim'                  => $hora_fim[$index],
                'duracao'                   => $dur[$index],
                'status'                    => 'A'
            ]);
        }

        return redirect()->route('agendamentos');
    }

    public function update(AgendamentoRequest $request)
    {
        $inicio = '' . $request->get('hora_inicio') . ':00';
        $recebe_duracao = new DateTime($request->get('duracao'));
        $duracao =  $recebe_duracao->format('H:i:s');
        $data = $request->get('data');
        $id = $request->get('id');
        $observacao = $request->get('observacao');
        $atendimento_servico_id = request()->atendimento_servico_id;
        $x = 0;

        $times = array($inicio, $duracao); //recebe a hora de inicio e a duracao em um array
        $seconds = 0;
        //soma a hora de inicio + duracao
        foreach ($times as $time) {
            list($hour, $minute, $second) = explode(':', $time);
            $seconds += $hour * 3600;
            $seconds += $minute * 60;
            $seconds += $second;
        }
        $hours = floor($seconds / 3600);
        $seconds -= $hours * 3600;
        $minutes  = floor($seconds / 60);
        $seconds -= $minutes * 60;
        //fim da soma

        // se passar da meia noite, adiciona um dia a mais na data do fim e ajusta a hora (para nao passar de 23:59)
        if ($hours >= 24) {
            $hours = $hours - 24;
            $data_fim = new DateTime($data);
            $data_fim = $data_fim->format("Y-m-d");
            $data_fim = date('Y-m-d', strtotime('+1 days', strtotime($data_fim)));
            $x = 1;
        } else {
            if ($x == 1) {
                $data = new DateTime($data);
                $data = $data->format("Y-m-d");
                $data = date('Y-m-d', strtotime('+1 days', strtotime($data)));
                $data_fim = new DateTime($data);
                $data_fim = $data_fim->format("Y-m-d");
                $x = 0;
            } else {
                $data_fim = new DateTime($data);
                $data_fim = $data_fim->format("Y-m-d");
            }
        }

        //corrige formato se for de 0 a 9 (evitar que fique 1:0:0 por exemplo... o certo é 01:00:00)
        if ($hours < 10) {
            $hours = '0' . $hours;
        }
        if ($minutes < 10) {
            $minutes = '0' . $minutes;
        }
        if ($seconds < 10) {
            $seconds = '0' . $seconds;
        }

        $resultado = '' . $hours . ':' . $minutes . ':' . $seconds; // resultado da soma (inicio + duracao) no formato 00:00:00 (H:m:s)

        $hora_inicio = '' . $data . ' ' . $inicio; //Deixar no formato "Y-m-d H:i:s" (padrão do fullcalendar)"
        $hora_fim = '' . $data_fim . ' ' . $resultado;
        $dur = '' . $duracao;


        Atendimento::where('id', '=', $id)
            ->update([
                'observacao' => $observacao
            ]);

        AtendimentoServico::where('id', '=', $atendimento_servico_id)
            ->update([
                'hora_inicio'               => $hora_inicio,
                'hora_fim'                  => $hora_fim,
                'duracao'                   => $dur
            ]);

        return redirect()->route('agendamentos');
    }

    public function delete(Request $request)
    {
        if ($request->ajax()) {
            try {
                $atendimento_id = AtendimentoServico::find($request->atendimento_servico)->get();

                foreach ($atendimento_id as $at) {
                    $id = $at->atendimento_id; // pega o id soh do atendimento
                }

                AtendimentoServico::find($request->atendimento_servico)->delete(); // apaga soh o servico/atendiemto

                $count = DB::table('atendimento_servico')
                    ->join('atendimentos', 'atendimentos.id', '=', 'atendimento_servico.atendimento_id')
                    ->where('atendimentos.id', '=', $id)
                    ->count(); // com o id do atendimento q foi pego antes... ele testa se o atendimento esta vazio..pq o servico acabou de ser apagado

                if ($count == 0) { // se nao tiver mais servicos.. apaga o atendimento tbm
                    Atendimento::find($id)->delete();
                }
                $ret = array('status' => 'ok', 'msg' => "null");
            } catch (\Illuminate\Database\QueryException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            } catch (\PDOException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            }
        }
        return $ret;
    }

    public function finalizaAtendimento(Request $request)
    {
        if ($request->ajax()) {
            try {
                AtendimentoServico::find($request->atendimento_servico)
                    ->update([
                        'status' => 'F'
                    ]);

                $atendimentoServico = DB::table('atendimentos')
                ->join('atendimento_servico', 'atendimentos.id', '=', 'atendimento_servico.atendimento_id')
                ->where('atendimento_servico.id', '=', $request->atendimento_servico)
                ->select('atendimento_servico.atendimento_id as atendimento_id')
                ->get();
                
                AtendimentoServico::find($request->atendimento_servico)->get('atendimento_id');
                foreach ($atendimentoServico as $at) {
                    $atendimento_id = $at->atendimento_id;
                }

                $atendimentos = DB::table('atendimentos')
                    ->join('atendimento_servico', 'atendimentos.id', '=', 'atendimento_servico.atendimento_id')
                    ->where('atendimentos.id', '=', $atendimento_id)
                    ->select('atendimento_servico.status as status')
                    ->get();

                $ativo = 0;

                foreach ($atendimentos as $aux) {
                    if ($aux->status == 'A') {
                        $ativo = 1;
                    }
                }

                if ($ativo != 1) {
                    Atendimento::find($atendimento_id)
                        ->update([
                            'status' => 'F'
                        ]);
                }

                $ret = array('status' => 'ok', 'msg' => "null");

            } catch (\Illuminate\Database\QueryException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            } catch (\PDOException $e) {
                $ret = array('status' => 'erro', 'msg' => $e->getMessage());
            }
            
            return $ret;
        }
    }
}
