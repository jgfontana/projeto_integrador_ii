<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => 'auth'], function () {

    Route::any('load-events', 'AgendamentosController@loadEvents')->name('routeLoadEvents');
        
    Route::group(['prefix' => 'agendamento', 'where' => ['id' => '[0-9]+']], function () {
        Route::any("",             ['as' => 'agendamentos',           'uses' => "AgendamentosController@index"]);
        Route::get("create",       ['as' => 'agendamentos.create',    'uses' => "AgendamentosController@create"]);
        Route::get("delete",       ['as' => 'agendamentos.destroy',   'uses' => "AgendamentosController@delete"]);
        Route::get("finaliza",     ['as' => 'agendamentos.finaliza',  'uses' => "AgendamentosController@finalizaAtendimento"]);
        Route::get("{id}/edit",    ['as' => 'agendamentos.edit',      'uses' => "AgendamentosController@edit"]);
        Route::put("update",       ['as' => 'agendamentos.update',    'uses' => "AgendamentosController@update"]);
        Route::post("store",       ['as' => 'agendamentos.store',     'uses' => "AgendamentosController@store"]);
        Route::get("profissionais",['as' => 'ag.profissionais',       'uses' => "AgendamentosController@getProfissionais"]);
        Route::get("servicos",     ['as' => 'ag.servicos',            'uses' => "AgendamentosController@getServicos"]);
        Route::get("soma-precos",  ['as' => 'ag.soma-precos',         'uses' => "AgendamentosController@getPrecos"]);
        Route::get("alerta",       ['as' => 'ag.alert',               'uses' => "AgendamentosController@getAlerta"]);
    });

    Route::group(['prefix' => 'clientes', 'where' => ['id' => '[0-9]+']], function () {
        Route::get("",             ['as' => 'clientes',           'uses' => "ClientesController@index"]);
        Route::get("{id}/destroy", ['as' => 'clientes.destroy',   'uses' => "ClientesController@destroy"]);
        Route::put("update",       ['as' => 'clientes.update',    'uses' => "ClientesController@update"]);
        Route::post("store",       ['as' => 'clientes.store',     'uses' => "ClientesController@store"]);
        Route::post("fast-store",  ['as' => 'clientes.fast-store','uses' => "ClientesController@storeFast"]);
    });

    Route::group(['prefix' => 'categorias-servico', 'where' => ['id' => '[0-9]+']], function () {
        Route::any("",             ['as' => 'categorias-servico',           'uses' => "ServicocategoriasController@index"]);
        Route::get("{id}/destroy", ['as' => 'categorias-servico.destroy',   'uses' => "ServicocategoriasController@destroy"]);
        Route::put("update",       ['as' => 'categorias-servico.update',    'uses' => "ServicocategoriasController@update"]);
        Route::post("store",       ['as' => 'categorias-servico.store',     'uses' => "ServicocategoriasController@store"]);
    });

    Route::group(['prefix' => 'servicos', 'where' => ['id' => '[0-9]+']], function () {
        Route::any("",             ['as' => 'servicos',           'uses' => "ServicosController@index"]);
        Route::get("{id}/destroy", ['as' => 'servicos.destroy',   'uses' => "ServicosController@destroy"]);
        Route::put("update",       ['as' => 'servicos.update',    'uses' => "ServicosController@update"]);
        Route::post("store",       ['as' => 'servicos.store',     'uses' => "ServicosController@store"]);
    });

    Route::group(['prefix' => 'profissionais', 'where' => ['id' => '[0-9]+']], function () {
        Route::get("",             ['as' => 'profissionais',           'uses' => "ProfissionaisController@index"]);
        Route::get("{id}/destroy", ['as' => 'profissionais.destroy',   'uses' => "ProfissionaisController@destroy"]);
        Route::put("update",       ['as' => 'profissionais.update',    'uses' => "ProfissionaisController@update"]);
        Route::post("store",       ['as' => 'profissionais.store',     'uses' => "ProfissionaisController@store"]);
    });

    Route::group(['prefix' => 'profissional-servico', 'where' => ['id' => '[0-9]+']], function () {
        Route::get("",             ['as' => 'profissional-servico',           'uses' => "ProfissionalServicoController@index"]);
        Route::get("{id}/destroy", ['as' => 'profissional-servico.destroy',   'uses' => "ProfissionalServicoController@destroy"]);
        Route::put("update",       ['as' => 'profissional-servico.update',    'uses' => "ProfissionalServicoController@update"]);
        Route::post("store",       ['as' => 'profissional-servico.store',     'uses' => "ProfissionalServicoController@store"]);
        Route::get("action",       ['as' => 'ps.action',                      'uses' => "ProfissionalServicoController@action"]);
    });

    Route::group(['prefix' => 'comandas', 'where' => ['id' => '[0-9]+']], function () {
        Route::get("",             ['as' => 'comandas',           'uses' => "ComandasController@index"]);
        Route::get("abertas",      ['as' => 'comandas.index2',    'uses' => "ComandasController@index2"]);
        Route::get("finalizadas",  ['as' => 'comandas.index3',    'uses' => "ComandasController@index3"]);
        Route::get("gera",         ['as' => 'comandas.gera',      'uses' => "ComandasController@geraComanda"]);
        Route::get("{id}/geraPDF",      ['as' => 'comandas.geraPDF',   'uses' => "ComandasController@geraRecibo"]);
        Route::get("servicos",     ['as' => 'comandas.servicos',  'uses' => "ComandasController@mostraServicos"]);
        Route::get("{id}/destroy", ['as' => 'comandas.destroy',   'uses' => "ComandasController@destroy"]);
        Route::put("update",       ['as' => 'comandas.update',    'uses' => "ComandasController@update"]);
        Route::post("store",       ['as' => 'comandas.store',     'uses' => "ComandasController@store"]);
    });

});