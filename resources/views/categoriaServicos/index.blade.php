@extends('adminlte::page')

@section('title', 'Categorias de Serviço')

@section('content_header')
<h1>Serviços</h1>
@stop

@section('content')
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a style="color: inherit" class="nav-item nav-link" id="nav-home-tab" href=" {{route('servicos')}} " role="tab"
            aria-controls="nav-home" aria-selected="false"><i class="fas fa-fw fa-handshake"></i> &nbsp;
            Cadastro de Serviços</a>
        <a style="color: inherit" class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href=""
            role="tab" aria-controls="nav-profile" aria-selected="true"><i class="fas fa-fw fa-th "></i> &nbsp;
            Cadastro Categorias de Serviço</a>
    </div>
</nav>
<div class="tab-content">
    <div class="container-fluid container-index-nav">
        <div class="row">
            <div class="col">
                <table class="table table-sm table-bordered table-striped" id="table_id">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Descrição</th>
                            <th style="width: 10px;"><i style="color: #141646" class="fas fa-edit"></i></th>
                            <th style="width: 10px;"><i style="color: #141646" class="fas fa-trash"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categoriaServicos as $categoria)
                        <tr>
                            <th scope="row"> {{ $categoria->id }} </th>
                            <td>{{ $categoria->nome }}</td>
                            <td>{{ $categoria->descricao }}</td>
                            <td style="text-align: center">
                                <a data-toggle="modal" data-target="#modalEditCategoriaServico" data-nome="{{$categoria->nome}}"
                                    data-descricao="{{$categoria->descricao}}" data-id="{{$categoria->id}}">
                                    <i style="color: royalblue; font-size: 20px" class="fas fa-edit"></i>
                                </a>
                            </td>
                            <td style="text-align: center">
                                <a onclick="return confirmaExclusao({{$categoria->id}})">
                                    <i style="color: darkred; font-size: 20px" class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<footer style="height: 30px">

</footer>

@include('categoriaServicos.create')
@include('categoriaServicos.edit')

@stop

@section('table-delete')
"categorias-servico"
@endsection

@section('footer')
&nbsp;
@endsection

@section('css')
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/servicocategorias/datatables.js')}}"></script>
<script type="text/javascript" src="{{asset('js/servicocategorias/edit.js')}}"></script>
@stop