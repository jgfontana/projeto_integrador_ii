<div class="modal fade create" id="modalCategoriaServico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nova Categoria</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route'=>'categorias-servico.store']) !!}
                @include('categoriaServicos.form')
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">Cancelar</button>
                    {!! Form::submit('Salvar Categoria', ['class'=>'btn btn-primary save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>