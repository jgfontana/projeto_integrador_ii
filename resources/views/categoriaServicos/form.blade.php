<div class="row">

    <div class="form-group col-md-12">
        {!! Form::label('nome', 'Nome*') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-th "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'maxlength' => '45',
            'placeholder'=> 'Nome da Categoria', 'name' => 'nome', 'required', 'minlength' => '3']) !!}
        </div>
    </div>

</div>

<div class="row">

    <div class="form-group col-md-12">
        {!! Form::label('descricao', 'Descrição') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-align-justify"></i>
                </span>
            </div>
            {!! Form::textarea(null, null, ['class'=>'form-control', 'placeholder'=> 'Informe a descrição da categoria',
            'name' => 'descricao', 'maxlength' => '255', 'rows' => 2])
            !!}
        </div>
    </div>

</div>