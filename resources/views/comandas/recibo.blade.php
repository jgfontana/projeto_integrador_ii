@php
use WGenial\NumeroPorExtenso\NumeroPorExtenso;
use Carbon\Carbon;
@endphp
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>R E C I B O</title>
    <style>
        @page {
            margin: 0px 50px 0px 50px !important;
            padding: 0px 0px 0px 0px !important;
        }

        body {
            font-family: 'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif
        }

        table {
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>

<body>
    <div style="text-align: left">
        <h6><b>Beauty</b>Soft</h6>
    </div>
    <div style="text-align: center; margin-top: -220px;">
        <h2 style="text-decoration: underline;">R E C I B O</h2>
    </div>
    <div style="text-align: center; font-size: 20px;">
        <p>Recebemos de <b>@foreach($cliente as $item){{$item->nome}}@endforeach</b> a quantia de <b>@foreach($comanda
                as $item)@php $extenso = new NumeroPorExtenso;$extenso = $extenso->converter($item->valor_final);echo
                $extenso;@endphp @endforeach</b>referente ao pagamento de:</p>
    </div>
    <table>
        <tr>
            <th>Serviço Prestado&nbsp;&nbsp;</th>
            <th>Valor</th>
        </tr>
        @foreach ($servicos as $item)
        <tr>
            <td>{{$item->servico}}</td>
            <td>R$ {{number_format($item->preco,2,",",".")}}</td>
        </tr>
        @endforeach
    </table>
    <br>
    <table>
        <tr>
            <th>Forma Pagamento</th>
            <th>Valor</th>
        </tr>
        @foreach ($formas as $item)
        <tr>
            <td>{{$item->forma}}</td>
            <td>R$ {{number_format($item->valor,2,",",".")}}</td>
        </tr>
        @endforeach
    </table>
    <br>
    <table>
        <tr>
            <th>Valor Total</th>
            <th>Desconto</th>
            <th>Valor Final</th>
            <th>Total Recebido</th>
            <th>Troco</th>
        </tr>
        @foreach ($comanda as $item)
        <tr>
            <td>R$ {{number_format($item->valor_total,2,",",".")}}</td>
            <td>{{$item->desconto}}%</td>
            <td>R$ {{number_format($item->valor_final,2,",",".")}}</td>
            <td>R$ {{number_format($item->recebido,2,",",".")}}</td>
            <td>R$ {{number_format($item->troco,2,",",".")}}</td>
        </tr>
        @endforeach
    </table>
    <div style="text-align: center">
        @foreach ($comanda as $item)
           @php
               
               setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
               date_default_timezone_set('America/Sao_Paulo');    
               $date = $item->data; 
        @endphp   
        <h4>Sarandi, @php
             echo strftime("%d de %B de %Y", strtotime( $date ));
        @endphp</h4>      
           
        @endforeach
    </div>
    <br>
    <div style="text-align: center;">
        <h3 style="text-decoration: overline;">JOAO GABRIEL FONTANA SOFTWARE LTDA</h3>
        <h6 style="margin-top: -10px;">Rua Tiradentes, 785 - Centro, Sarandi, RS</h6>
        <h6 style="margin-top: -20px;">CNPJ: 23.947.157/0001-24</h6>
    </div>
    <hr style="border-top: 1px dashed black;">
</body>

</html>