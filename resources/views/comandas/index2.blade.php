@extends('adminlte::page')

@section('title', 'Comandas')

@section('content_header')
<h1>Comandas</h1>
@stop

@section('content')
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a style="color: inherit" class="nav-item nav-link" id="nav-profile-tab" href=" {{route('comandas')}} "
            role="tab" aria-controls="nav-profile" aria-selected="false"><i style="color: rgb(68, 28, 80)"
                class="fas fa-fw fa-clock "></i> &nbsp;
            Atendimentos</a>
        <a style="color: inherit" class="nav-item nav-link active" id="nav-profile-tab" href="" role="tab"
            aria-controls="nav-profile" aria-selected="false"><i style="color: orange" class="fas fa-fw fa-flag "></i>
            &nbsp; Comandas Abertas</a>
        <a style="color: inherit" class="nav-item nav-link" id="nav-profile-tab"  href=" {{route('comandas.index3')}} " role="tab"
            aria-controls="nav-profile" aria-selected="false"><i style="color: darkgreen"
                class="fas fa-fw fa-flag "></i> &nbsp; Comandas Finalizadas</a>
    </div>
</nav>
<div class="tab-content">
    <div class="container-fluid container-index-nav">
        <table class="table table-sm table-bordered table-striped" id="table_id">
            <thead class="thead-dark">
                <tr style="text-align: center">
                    <th>#</th>
                    <th>Data</th>
                    <th>Cliente</th>
                    <th>Valor Total</th>
                    <th>Finalizar</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($atendimentos as $atend)
                <tr>
                    <td style="text-align: center">{{ $atend->numero }}</td>
                    <td style="text-align: center">{{ Carbon\Carbon::parse($atend->data)->format('d/m/Y') }}</td>
                    <td>{{ $atend->cliente }}</td>
                    <td style="text-align: left">R$ {{ number_format($atend->total,2,",",".") }}</td>
                    <td id="gera-icon">
                        <a data-toggle="modal" data-target="#modalComanda" class="btn btn-sm btn-success"
                            data-cliente="{{$atend->cliente}}" data-numero="{{$atend->numero}}"
                            data-dia="{{Carbon\Carbon::parse($atend->data)->format('Y-m-d')}}"
                            data-valor_total="{{$atend->total}}" data-id="{{$atend->id}}"
                            data-atendimento_id="{{$atend->atendimento_id}}">
                            <i class="fas fa-dollar-sign"></i> Pagar
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<footer style="height: 30px">

</footer>
@include('comandas.modal')
@stop

@section('table-delete')
"clientes"
@endsection

@section('footer')
&nbsp;
@endsection

@section('css')
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/comandas/datatables2.js')}}"></script>
<script type="text/javascript" src="{{asset('js/comandas/modal.js')}}"></script>
<script>
$(document).ready(function(){
$('#desconto').mask('00');
$('.valor').mask('000.000.000.000.000,00', {reverse: true});
});
</script>
<script>
$('#modalComanda').on('show.bs.modal', function () {
    $('#tbody').empty();
    var atendimento_id = $('#atendimento_id').val();
    $.get('servicos', { atendimento_id: atendimento_id }, function (servicos) {
        var array = {};
        $.each(servicos, function (index, value) {  
            array += '<tr><td>'+value.servico+'</td><td>R$ '+value.preco.toFixed(2).replace(".", ",");+'</td></tr>';
        })
        $('#tbody').append(array);
    });
})
</script>
<script>
$(document).ready(function () {
	$("#desconto").on('keyup', function () {

		var total = parseFloat($('#total').val()) || 0;
		var desconto = parseFloat($('#desconto').val()) || 0;

		var valor_final = total - (total / 100 * desconto);

        $('#final').val(valor_final.toFixed(2).replace(".", ","));
        $('.valor').blur();
	});
});
</script>
<script>
$(document).ready(function () {
    var wrapper = $("#add_form_pag");
    var x = 1;
    $('#btn_add').click(function (e) {
		var check_exist_valor = {};
        var existe = 0;
		$.each($('.valor'), function () {
            check_exist_valor = $(this).val();
            if(check_exist_valor == ''){
                existe = 1;
            }
		});
		if (existe == 1) {
			Swal.fire('Verifique se o campo valor foi informado corretamente antes de adicionar outro!');
		} else {
			var x_old = x;
            x++;
			var newField = '<div style="margin-top: -10px" class="rem form-row show' + x + '"> <div class="form-group col-md-5"> <label>Forma de Pagamento</label> <div class="input-group mb-3"> <div class="input-group-prepend"> <span class="input-group-text icon-input" id="basic-addon1"> <i class="fas fa-fw fa-money-bill"></i> </span> </div> <select name="formaPagamento_id[]" class="form-control forma' + x + '" required> @foreach ($formas as $item) <option value="{{$item->id}}">{{$item->nome}}</option> @endforeach </select> </div> </div> <div class="form-group col-md-4"> <label>Valor</label> <div class="input-group mb-3"> <div class="input-group-prepend"> </div> <input type="text" onblur="soma();" class="form-control valor valor' + x + '" required name="valor[]"> </div> </div> <div class="form-group col-md-2"><button type="button" class="btn btn-sm remove_field"><i style="color: red" class="fas fa-times"></i> Remover</button></div> </div>';
            $(wrapper).append(newField);
            $('.valor'+x).mask('000.000.000.000.000,00', {reverse: true});
			$('.show' + x).hide().show('slow');
			jQuery.fn.fadeOutAndRemove = function (speed) {
				$(this).hide(speed, function () {
					$(this).remove();
					soma();
					x--;
				})
			}
		}
    });
    $(wrapper).on("click", ".remove_field", function (e) {
		e.preventDefault();
		$(this).parents('.rem').fadeOutAndRemove('slow');
	});
}); 
</script>
<script>
function soma() {
        var valor = $('.valor').map(function () {
		    return this.value;
        }).get();
        
        var soma = 0;

        valor.forEach(function(item) {
            soma = soma + parseFloat(item.replace('.','').replace(',','.')) || 0;
        });

        $('#recebido').val(soma.toLocaleString('pt-br', {minimumFractionDigits: 2}));
        $('#recebido').change();
}
</script>
<script>
$('#recebido').on('change', function() {
    var valor_final = parseFloat($('#final').val().replace('.','').replace(',','.')) || 0;
    var recebido = parseFloat($('#recebido').val().replace('.','').replace(',','.')) || 0;

    var soma = valor_final - recebido;

    if(soma < 0){
        var vazio = 0.00;
        $('#restante').val(vazio.toLocaleString('pt-br', {minimumFractionDigits: 2}));
        soma = Math.abs(soma);
        soma = soma.toLocaleString('pt-br', {minimumFractionDigits: 2});
        $('#troco').val(soma);
    }else{
        var vazio = 0.00;
        $('#troco').val(vazio.toLocaleString('pt-br', {minimumFractionDigits: 2}));
        soma = soma.toLocaleString('pt-br', {minimumFractionDigits: 2});
        $('#restante').val(soma);
    }
});
</script>
<script>
function valida() {
    var restante = parseFloat($('#restante').val().replace('.','').replace(',','.')) || 0;

    if (restante > 0) {
        Swal.fire({
            icon: 'warning',
            title: 'Oops...',
            text: 'Ainda falta informar pagamentos!',
        })
        return false;
    } else {
        return true;
    }
}
</script>
@stop