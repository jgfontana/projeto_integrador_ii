@extends('adminlte::page')

@section('title', 'Comandas')

@section('content_header')
<h1>Comandas</h1>
@stop

@section('content')
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a style="color: inherit" class="nav-item nav-link" id="nav-profile-tab" href=" {{route('comandas')}} "
            role="tab" aria-controls="nav-profile" aria-selected="false"><i style="color: rgb(68, 28, 80)"
                class="fas fa-fw fa-clock "></i> &nbsp;
            Atendimentos</a>
        <a style="color: inherit" class="nav-item nav-link" id="nav-profile-tab" href=" {{route('comandas.index2')}} " role="tab"
            aria-controls="nav-profile" aria-selected="false"><i style="color: orange" class="fas fa-fw fa-flag "></i>
            &nbsp; Comandas Abertas</a>
        <a style="color: inherit" class="nav-item nav-link active" id="nav-profile-tab" href="" role="tab"
            aria-controls="nav-profile" aria-selected="false"><i style="color: darkgreen"
                class="fas fa-fw fa-flag "></i> &nbsp; Comandas Finalizadas</a>
    </div>
</nav>
<div class="tab-content">
    <div class="container-fluid container-index-nav">
        <table class="table table-sm table-bordered table-striped" id="table_id">
            <thead class="thead-dark">
                <tr style="text-align: center">
                    <th>#</th>
                    <th>Data</th>
                    <th>Cliente</th>
                    <th>Total</th>
                    <th>Desconto</th>
                    <th>Recebido</th>
                    <th>Troco Cliente</th>
                    <th>Final</th>
                    <th>Recibo</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($atendimentos as $atend)
                <tr>
                    <td style="text-align: center">{{ $atend->numero }}</td>
                    <td style="text-align: center">{{ Carbon\Carbon::parse($atend->data)->format('d/m/Y') }}</td>
                    <td>{{ $atend->cliente }}</td>
                    <td style="text-align: left">R$ {{ number_format($atend->total,2,",",".") }}</td>
                    <td style="text-align: center">{{ $atend->desconto }}%</td>
                    <td style="text-align: left">R$ {{ number_format($atend->recebido,2,",",".") }}</td>
                    <td style="text-align: left">R$ {{ number_format($atend->troco,2,",",".") }}</td>
                    <td style="text-align: left">R$ {{ number_format($atend->final,2,",",".") }}</td>
                    <td id="gera-icon">
                        <a target="_blank" href="{{$atend->id}}/geraPDF" class="btn btn-sm btn-primary">
                            <i class="fas fa-print"></i>
                             Gerar
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<footer style="height: 30px">

</footer>
@include('comandas.modal')
@stop

@section('table-delete')
"clientes"
@endsection

@section('footer')
&nbsp;
@endsection

@section('css')
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/comandas/datatables3.js')}}"></script>
@stop