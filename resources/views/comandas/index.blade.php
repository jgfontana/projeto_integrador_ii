@extends('adminlte::page')

@section('title', 'Comandas')

@section('content_header')
<h1>Comandas</h1>
@stop

@section('content')
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a style="color: inherit" class="nav-item nav-link active" id="nav-profile-tab"
             role="tab" aria-controls="nav-profile" aria-selected="false"><i
                style="color: rgb(68, 28, 80)" class="fas fa-fw fa-clock "></i> &nbsp;
            Atendimentos</a>
        <a style="color: inherit" href="{{route('comandas.index2')}}" class="nav-item nav-link" id="nav-profile-tab" href="comandas" role="tab"
            aria-controls="nav-profile" aria-selected="false"><i style="color: orange" class="fas fa-fw fa-flag "></i>
            &nbsp; Comandas Abertas</a>
        <a style="color: inherit" class="nav-item nav-link" id="nav-profile-tab"  href=" {{route('comandas.index3')}} " role="tab"
            aria-controls="nav-profile" aria-selected="false"><i style="color: darkgreen"
                class="fas fa-fw fa-flag "></i> &nbsp; Comandas Finalizadas</a>
    </div>
</nav>
<div class="tab-content">
    <div class="container-fluid container-index-nav">
        <table class="table table-sm table-bordered table-striped" id="table_id">
            <thead class="thead-dark">
                <tr style="text-align: center">
                    <th>#</th>
                    <th>Data</th>
                    <th>Cliente</th>
                    <th>Status</th>
                    <th>Gerar Comanda</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($atendimentos as $atend)
                <tr>
                    <th style="text-align: center">{{ $atend->id }}</th>
                    <td style="text-align: center">{{ Carbon\Carbon::parse($atend->data)->format('d/m/Y') }}</td>
                    <td>{{ $atend->cliente }}</td>
                    <td style="text-align: center">@if($atend->status == 'A') Aguardando Finalização @elseif($atend->status == 'F') Finalizado @endif</td>
                    <td id="gera-icon">
                        @if ($atend->status == 'F')
                        <a onclick="abreComanda({{$atend->id}});" class="btn btn-sm btn-primary">
                            <i class="fas fa-dollar-sign"></i> Gerar Comanda
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<footer style="height: 30px">

</footer>
@stop

@section('table-delete')
"clientes"
@endsection

@section('footer')
&nbsp;
@endsection

@section('css')
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/comandas/datatables.js')}}"></script>
<script>
function abreComanda(id) {
    Swal.fire({
		title: 'Abrir Comanda?',
		imageUrl: "http://127.0.0.1:8000/vendor/sweetalert2/comanda.png",
		backdrop: 'rgba(20,22,70,0.4)',
		imageWidth: 100,
		imageHeight: 100,
		showCancelButton: true,
		closeOnConfirm: false,
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Sim, abrir comanda!',
	}).then((result) => {
		if (result.value) {
			$.get('comandas/gera', {
				id: id
			}, function (s) {
				if (s.status == "ok") {
					Swal.fire(
						'Comanda aberta!',
						'Vá até "Comandas Abertas" para finalizá-la',
						'success'
					).then(function (isConfirm) {
						window.location.href = "{{route('comandas.index2')}}";
					});
				} else
					Swal.fire(
						'Erro!',
						'Ocorreram erros na abertura da comanda!. Entre em contato com o suporte.',
						'error'
					);
			});
		}
	})
}    
</script>
@stop