<input type="hidden" id="id" name="id" value="">
<input type="hidden" id="atendimento_id" name="atendimento_id" value="">
<div class="row" style="margin-top: -10px">
    <div class="form-group col-md-6">
        {!! Form::label('cliente_id', 'Cliente') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-user "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'name' => 'cliente_id', 'required',
            'readonly'=>'readonly']) !!}
        </div>
    </div>

    <div class="form-group col-md-2">
        {!! Form::label('numero', 'Nº Comanda') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-hashtag"></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'readonly' => 'readonly' ,'name' => 'numero']) !!}
        </div>
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('data', 'Data') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-calendar "></i>
                </span>
            </div>
            {!! Form::date(null, null, ['class'=>'form-control', 'name'=>'data', 'readonly' => 'readonly']) !!}
        </div>
    </div>

</div>
<hr style="margin-top: -20px">
<div class="row">
    <div style="margin-top: -20px" class="form-group col-md-12">
        <table class="table table-sm table-borderless table-striped">
            <thead>
                <tr>
                    <th scope="col">Serviço(s) Prestado(s)</th>
                    <th scope="col">Valor</th>
                </tr>
            </thead>
            <tbody id="tbody">

            </tbody>
        </table>
    </div>
</div>
<hr style="margin-top: -20px">
<div class="row" style="margin-top: -10px">
    <div class="form-group col-md-4">
        {!! Form::label('valor_total', 'Valor Total do(s) Serviço(s)') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-dollar-sign "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'id'=>'total', 'name'=>'valor_total', 'readonly' =>
            'readonly']) !!}
        </div>
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('desconto', 'Desconto (%)') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-percentage "></i>
                </span>
            </div>
            {!! Form::text(null, 0, ['class'=>'form-control', 'id'=>'desconto', 'name'=>'desconto']) !!}
        </div>
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('valor_final', 'Valor Final') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-dollar-sign "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'readonly' => 'readonly',
            'id'=>'final','name'=>'valor_final']) !!}
        </div>
    </div>
</div>
<hr style="margin-top: -20px">
<div class="row" style="margin-top: -10px">

    <div class="form-group col-md-5">
        <label>Forma de Pagamento</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-fw fa-money-bill"></i>
                </span>
            </div>
            <select name="formaPagamento_id[]" class="form-control forma1" required>
                @foreach ($formas as $item)
                <option value="{{$item->id}}">{{$item->nome}}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="form-group col-md-4">
        <label>Valor</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            </div>
            <input type="text" onblur="soma();" class="form-control valor valor1" required name="valor[]">
        </div>
    </div>

    <div class="form-group col-md-2 btn-add-hide">
        <button type="button" class="btn btn-sm add" id="btn_add"><i style="color: green" class="fas fa-plus"></i>
            Adicionar</button>
    </div>
</div>

<div id="add_form_pag" style="margin-top: -10px">

</div>

<hr style="margin-top: -20px">
<div class="row" style="margin-top: -10px">

    <div class="form-group col-md-4">
        <label for="">Falta receber</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            </div>
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-fw fa-funnel-dollar"></i>
                </span>
            </div>
            <input type="text" readonly id="restante" value="" class="form-control">
        </div>
    </div>

    <div class="form-group col-md-4">
        <label for="">Valor recebido</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            </div>
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-fw fa-dollar-sign"></i>
                </span>
            </div>
            <input type="text" readonly="readonly" id="recebido" name="recebido" value="" class="form-control">
        </div>
    </div>

    <div class="form-group col-md-4">
        <label for="">Dar de troco</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
            </div>
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-fw fa-hand-holding-usd"></i>
                </span>
            </div>
            <input type="text" readonly="readonly" id="troco" name="troco" value="" class="form-control">
        </div>
    </div>
</div>