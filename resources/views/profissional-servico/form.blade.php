<div class="row" hidden>

    <div class="form-group col-md-12">
        {!! Form::label('nome_servico', 'Profissional') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-id-card "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'name' => 'profissional_id', 'id' =>
            'profissional_id']) !!}
        </div>
    </div>

</div>

<div hidden>
    {!! Form::text(null, 'A', ['name' => 'status', 'id' => 'status']) !!}
</div>

<div class="row">

    <div class="form-group col-md-12">
        <label>Serviço a ser adicionado:</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-handshake "></i>
                </span>
            </div>
            <select class="selectpicker form-control" data-live-search="true" name="servico_id">
                <option style="font-size: 0px;" selected disabled>Informe o Serviço</option>
                {{ $max = 10 }}
                @for ($id = 1; $id <= $max; $id++) @foreach ($servicos as $servico) @if ($servico->id_categoria == $id)
                    @foreach ($servicos as $servico)
                    @if ($servico->id_categoria == $id)
                    <optgroup label=" {{ $servico->nome_categoria }} ">
                        @break
                        @endif
                        @endforeach
                        @foreach ($servicos as $servico)
                        @if ($servico->id_categoria == $id)
                        <option value="{{ $servico->id }}">{{ $servico->nome }}</option>
                        @endif
                        @endforeach
                        @break
                        @endif
                        @endforeach
                        @endfor
            </select>
        </div>
    </div>

</div>