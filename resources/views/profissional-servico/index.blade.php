@extends('adminlte::page')

@section('title', 'Profissionais')

@section('content_header')
<h1>Profissionais</h1>
@stop

@section('content')
<nav>
  <div class="nav nav-tabs" id="nav-tab" role="tablist">
    <a style="color: inherit" class="nav-item nav-link" id="nav-profile-tab" href=" {{route('profissionais')}} "
      role="tab" aria-controls="nav-profile" aria-selected="false"><i class="fas fa-fw fa-id-card "></i> &nbsp;
      Cadastro de Profissionais</a>
    <a style="color: inherit" class="nav-item nav-link active" id="nav-profile-tab"
      href=" {{route('profissional-servico')}} " role="tab" aria-controls="nav-profile" aria-selected="false"><i
        class="fas fa-fw fa-user-cog "></i> &nbsp; Serviço exercido por profissional</a>
  </div>
</nav>
<div class="tab-content">
  <div class="container-fluid container-index-nav">
    <div class="row">

      <div class="col-3">
        <div class="card card-master" style="color: #292d31">
          <div class="card-header" style="text-align: center; font-weight: bold">
            Profissional
          </div>
          <div class="container-fluid">
            <select size="{{$count}}" name="profissional_id select_prof" id="profissional_id" class="select-master"
              onchange="mostraNome()">
              @foreach ($profissionais as $p)
              <option value="{{ $p->id }}">{{ $p->nome }}</option>
              @endforeach
            </select>
          </div>
        </div>
      </div>

      <div class="col-1">
        <div class="arrow">
          <i class="fas fa-exchange-alt"></i>
        </div>
      </div>

      <div class="col-8">
        <div class="card card-master" style="color: #292d31">
          <div class="card-header" style="text-align: center; font-weight: bold">
            Serviço(s) <span id="live-nome"></span>


            <button hidden style="float: right;" class="btn btn-sm" id="novo-master" type="submit" data-toggle="modal"
              data-target="#modalProfserv">
              Adicionar
              <i class="fas fa-plus-circle"></i>
            </button>


          </div>
          <div class="container-fluid">

            <table class="table table-sm table-hover table-bordered table-master" id="table_id">
              <thead class="thead">
                <tr>
                  <th scope="col">Nome</th>
                  <th scope="col">Categoria</th>
                  <th style="width: 10px;"></th>
                </tr>
              </thead>
              <tbody id="tabela">
                <tr id="msg">
                  <td colspan="3" style="text-align: center">Selecione um Profissional</td>
                </tr>
              </tbody>
            </table>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<footer style="height: 30px">

</footer>

@section('table-delete')
"profissional-servico"
@endsection

@include('profissional-servico.create')

@stop

@section('footer')
&nbsp;
@endsection

@section('css')
@stop

@section('js')

<script type="text/javascript" src="{{asset('js/profissional-servico/view.js')}}"></script>

<script>
  function mostraNome() {
    var x = $("#profissional_id option:selected").text();
    document.getElementById("live-nome").innerHTML = "de: " + x;
  }
</script>

<script>
$(document).on('change', '#profissional_id', function () {
    var query = $(this).val();
    var profissional = document.getElementById("profissional_id").value;
    var dados = { query: query, profissional };
    $.ajax({
        url: "{{ route('ps.action') }}",
        method: 'GET',
        data: dados,
        dataType: 'json',
        success: function (data) {
            $('#tabela').html(data.table_data);
            $("#msg").hide();
            $("#novo-master").removeAttr("hidden");
          }
      })
});
</script>

<script>
  var msg1 = '{{Session::get('alert1')}}';
  var exist1 = '{{Session::has('alert1')}}';

  var msg2 = '{{Session::get('alert2')}}';
  var exist2 = '{{Session::has('alert2')}}';

  var prof = '{{Session::get('prof')}}'
  var exist_prof = '{{Session::has('prof')}}'

  if(exist1){
    window.onload = initPage;
    function initPage(){
      Swal.fire({
        icon: 'info',
        title: msg1,
        showConfirmButton: false,
        timer: 3000        
      })
    }
  }

  if(exist2){
    window.onload = initPage;
    function initPage(){
      Swal.fire({
        icon: 'success',
        title: msg2,
        showConfirmButton: false,
        timer: 1500  
      })
    }
  }

  if(exist_prof){
    $("#profissional_id").val(prof).change();
  }
</script>
@stop