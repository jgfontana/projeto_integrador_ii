<div class="modal fade create" id="modalServico" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Novo Serviço <i style="font-size: 15px">&nbsp;&nbsp;
                        Campos com * são obrigatórios</i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route'=>'servicos.store']) !!}
                @include('servicos.form')
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">Cancelar</button>
                    {!! Form::submit('Salvar Serviço', ['class'=>'btn btn-primary save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>