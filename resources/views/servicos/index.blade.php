@extends('adminlte::page')

@section('title', 'Serviços')

@section('content_header')
<h1>Serviços</h1>
@stop

@section('content')
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a style="color: inherit" class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href=""
            role="tab" aria-controls="nav-home" aria-selected="true"><i class="fas fa-fw fa-handshake"></i> &nbsp;
            Cadastro de Serviços</a>
        <a style="color: inherit" class="nav-item nav-link" id="nav-profile-tab"
            href=" {{route('categorias-servico')}} " role="tab" aria-controls="nav-profile" aria-selected="false"><i
                class="fas fa-fw fa-th "></i> &nbsp; Cadastro Categorias de Serviço</a>
    </div>
</nav>
<div class="tab-content">
    <div class="container-fluid container-index-nav">
        <div class="row">
            <div class="col">
                <table class="table table-sm table-bordered table-striped" id="table_id">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">Duração (H:m)</th>
                            <th scope="col">Preço</th>
                            <th scope="col">Categoria</th>
                            <th style="width: 10px;"><i style="color: #141646" class="fas fa-eye"></i></th>
                            <th style="width: 10px;"><i style="color: #141646" class="fas fa-edit"></i></th>
                            <th style="width: 10px;"><i style="color: #141646" class="fas fa-trash"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($servicos as $servico)
                        <tr>
                            <th scope="row"> {{ $servico->id }} </th>
                            <td>{{ $servico->nome_servico }}</td>
                            <td>{{ $servico->duracao }}</td>
                            <td>R$ {{ $servico->preco }}</td>
                            <td>{{ $servico->categoria->nome }}</td>
                            <td style="text-align: center">
                                <a data-toggle="modal" data-target="#modalViewServico"
                                    data-nome_servico="{{$servico->nome_servico}}" data-duracao="{{$servico->duracao}}"
                                    data-descricao="{{$servico->descricao}}" data-preco="{{$servico->preco}}"
                                    data-categoria_id="{{$servico->categoria_id}}">
                                    <i style="color: black; font-size: 20px;" class="fas fa-eye"></i>
                                </a>
                            </td>
                            <td style="text-align: center">
                                <a data-toggle="modal" data-target="#modalEditServico"
                                    data-nome_servico="{{$servico->nome_servico}}" data-duracao="{{$servico->duracao}}"
                                    data-descricao="{{$servico->descricao}}" data-preco="{{$servico->preco}}"
                                    data-categoria_id="{{$servico->categoria_id}}" data-id="{{$servico->id}}">
                                    <i style="color: royalblue; font-size: 20px" class="fas fa-edit"></i>
                                </a>
                            </td>
                            <td style="text-align: center">
                                <a onclick="return confirmaExclusao({{$servico->id}})">
                                    <i style="color: darkred; font-size: 20px" class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<footer style="height: 30px">

</footer>

@include('servicos.create')
@include('servicos.edit')
@include('servicos.view')

@stop

@section('table-delete')
"servicos"
@endsection

@section('footer')
&nbsp;
@endsection

@section('css')
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/servicos/datatables.js')}}"></script>
<script type="text/javascript" src="{{asset('js/servicos/view.js')}}"></script>
<script type="text/javascript" src="{{asset('js/servicos/edit.js')}}"></script>
@stop