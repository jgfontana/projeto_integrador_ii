<div class="row">

    <div class="form-group col-md-8">
        {!! Form::label('nome_servico', 'Nome do Serviço*') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-user "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'maxlength' => '45',
            'placeholder'=> 'Nome do Serviço', 'name' => 'nome_servico', 'required', 'minlength' => '3']) !!}
        </div>
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('duracao', 'Duração*&nbsp;') !!}
        <a class="tooltip-test" title="Tempo aproximado de execução do serviço."><i
                class="fas fa-question-circle"></i></a>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-hourglass-half"></i>
                </span>
            </div>
            <input type="time" class="form-control" name="duracao" required>
        </div>
    </div>

</div>

<div class="row">

    <div class="form-group col-md-6">
        {!! Form::label('categoria_id', 'Categoria*') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-fw fa-th"></i>
                </span>
            </div>
            {!! Form::select(null, \App\Servicocategoria::orderBy('nome')->pluck('nome', 'id')->toArray(), null,
            ['class'=>'form-control', 'require', 'name'=>'categoria_id']) !!}
        </div>
    </div>

    <div class="form-group col-md-6">
        {!! Form::label('preco', 'Valor*') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-dollar-sign"></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control preco', 'placeholder'=> '0,00', 'name' => 'preco', 'required'])
            !!}
        </div>
    </div>

</div>

<div class="row">

    <div class="form-group col-md-12">
        {!! Form::label('descricao', 'Descrição') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-align-justify"></i>
                </span>
            </div>
            {!! Form::textarea(null, null, ['class'=>'form-control', 'placeholder'=> 'Informe a descrição do serviço',
            'name' => 'descricao', 'maxlength' => '255', 'rows' => 1])
            !!}
        </div>
    </div>

</div>