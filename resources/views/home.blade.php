@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h5><i class="fas fa-fw fa-tachometer-alt"></i> Dashboard</h5>
@stop

@section('content')
<div class="container-fluid" style="margin: 0%">
    <div class="row">
        <div class="col-lg-12 col-12" style="text-align: center">
            <h5>Dados de Hoje:</h5>
        </div>
    </div>
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
                <div class="inner">
                    <h3>{{$atendimentos}}</h3>

                    <p>Agendamentos</p>
                </div>
                <div class="icon">
                    <i style="color: #2f2a60;" class="fas fa-calendar-day"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
                <div class="inner">
                    <h3>@foreach($faturamento as $item){{ number_format($item->total,2,",",".") }}@endforeach</h3>

                    <p>Faturamento do dia</p>
                </div>
                <div class="icon">
                    <i style="color: #141646" class="fas fa-dollar-sign"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
                <div class="inner">
                    <h3>{{$comandas}}</h3>

                    <p>Comandas</p>
                </div>
                <div class="icon">
                    <i style="background: -webkit-linear-gradient(-140deg , #90697b, #141646 90%, #141646); -webkit-background-clip: text; -webkit-text-fill-color: transparent;"
                        class="fas fa-fw fa-receipt"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
                <div class="inner">
                    <h3>{{$clientes}}</h3>

                    <p>Clientes novos</p>
                </div>
                <div class="icon">
                    <i style="color: #90697b" class="fas fa-users"></i>
                </div>
            </div>
        </div>
        <!-- ./col -->
    </div>
</div>
<hr>
<div class="container-fluid" style="margin-top: 10px;">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-light" style="background: white">
                <div class="card-header">
                    <h3 class="card-title">Faturamento por dia</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="faturamentoPorDia"
                            style="min-height: 250px; height: 500px; max-height: 500px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-light" style="background: white">
                <div class="card-header">
                    <h3 class="card-title">Atendimentos finalizados</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="finalizadoPorProfissional"
                            style="min-height: 250px; height: 650px; max-height: 650px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-light" style="background: white">
                <div class="card-header">
                    <h3 class="card-title">Agendamentos em aberto</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="agendamentoAberto"
                            style="min-height: 250px; height: 650px; max-height: 650px; max-width: 100%;"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('footer')
&nbsp;
@endsection

@section('css')
@stop
@section('js')
<script>
    var ctx = document.getElementById('finalizadoPorProfissional').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: [@php echo $result_nome_at_finalizado; @endphp],
            datasets: [{
                data: [{{implode(',', $qtd_at_finalizado)}}],
                backgroundColor: [@php echo $result_cor_at_finalizado; @endphp],
            }]
        },
    });

    var ctx = document.getElementById('agendamentoAberto').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            labels: [@php echo $result_nome_ag_aberto; @endphp],
            datasets: [{
                data: [{{implode(',', $qtd_ag_aberto)}}],
                backgroundColor: [@php echo $result_cor_ag_aberto; @endphp],
            }]
        },
    });

    var ctx = document.getElementById('faturamentoPorDia').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [@php echo $result_dia; @endphp],
            datasets: [{
                label: 'Faturamento',
                data: [@php echo $result_valor; @endphp],
            }]
        },
    });
</script>
@stop