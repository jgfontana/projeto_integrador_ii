<div class="modal subModal" tabindex="-1" id="subModal" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cadastro rápido de cliente</h5>
            </div>
            <div class="modal-body">
                {!! Form::open(['route'=>'clientes.fast-store']) !!}
                <div class="row">
                    <div class="form-group col-md-12">
                        {!! Form::label('nome', 'Nome*') !!}
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text icon-input" id="basic-addon1">
                                    <i class="fas fa-user "></i>
                                </span>
                            </div>
                            {!! Form::text(null, null, ['class'=>'form-control', 'maxlength' =>
                            '100',
                            'placeholder'=> 'Nome do Cliente', 'name' => 'nome-cliente', 'required', 'minlength' => '3']) !!}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-12">
                        {!! Form::label('sexo', 'Gênero*') !!}
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text icon-input" id="basic-addon1">
                                    <i class="fas fa-venus-mars "></i>
                                </span>
                            </div>
                            <select name="sexo" class="form-control" required>
                                <option selected disabled value="">Selecione</option>
                                <option value="F">Feminino</option>
                                <option value="M">Masculino</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="sair" class="btn btn-secondary cancel">Cancelar</button>
                {!! Form::submit('Salvar Cliente', ['class'=>'btn btn-primary save']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>