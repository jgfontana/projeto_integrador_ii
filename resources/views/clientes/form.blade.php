<div class="row">

    <div class="form-group col-md-5">
        {!! Form::label('nome', 'Nome*') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-user "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'maxlength' => '100',
            'placeholder'=> 'Nome do Cliente', 'name' => 'nome', 'required', 'minlength' => '3']) !!}
        </div>
    </div>

    <div class="form-group col-md-3">
        {!! Form::label('nasc', 'Data Nascimento') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-calendar "></i>
                </span>
            </div>
            {!! Form::date(null, null, ['class'=>'form-control', 'name'=>'nasc']) !!}
        </div>
    </div>

    <div class="form-group col-md-2">
        {!! Form::label('cpf', 'CPF') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-id-badge "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control cpf', 'maxlength' => '14' ,'name' =>
            'cpf','placeholder'=> '000.000.000-00']) !!}
        </div>
    </div>

    <div class="form-group col-md-2">
        {!! Form::label('sexo', 'Gênero*') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-venus-mars "></i>
                </span>
            </div>
            <select name="sexo" class="form-control" required>
                <option selected disabled value="">Selecione</option>
                <option value="F">Feminino</option>
                <option value="M">Masculino</option>
            </select>
        </div>
    </div>

</div>

<div class="row">

    <div class="form-group col-md-2">
        {!! Form::label('celular', 'Celular') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-mobile "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control celular', 'name' => 'celular',
            'maxlength' => '15', 'placeholder'=> '(00) 00000-0000']) !!}
        </div>
    </div>

    <div class="form-group col-md-2">
        {!! Form::label('fone', 'Telefone') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-phone-alt "></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control fone',
            'placeholder'=> '(00) 0000-0000', 'name' => 'fone']) !!}
        </div>
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('email', 'E-mail') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-at "></i>
                </span>
            </div>
            {!! Form::email(null, null, ['class'=>'form-control', 'maxlength' => '100',
            'placeholder'=> 'email@email.com.br', 'name' => 'email']) !!}
        </div>
    </div>

    <div class="form-group col-md-3">
        {!! Form::label('alergia', 'É alérgico(a)?*&nbsp;&nbsp;') !!}
        <a href="#" class="tooltip-test"
            title="Se o cliente possuir algum tipo de alergia, um alerta será apresentando na tela no momento de um agendamento."><i
                class="fas fa-question-circle"></i></a>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-allergies"></i>
                </span>
            </div>
            <select name="alergia" class="form-control alergia" required>
                <option selected disabled value="">Selecione</option>
                <option value="1">Sim</option>
                <option value="0">Não</option>
            </select>
        </div>
    </div>

</div>

<div class="row desc">

    <div class="form-group col-md-12">
        {!! Form::label('descricao', 'Descrição*') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-comment-medical"></i>
                </span>
            </div>
            {!! Form::textarea(null , null, ['class'=>'form-control descricao', 'placeholder'=>
            'Descreva a alergia do cliente aqui...', 'rows' => 1, 'name' => 'descricao']) !!}
        </div>
    </div>
</div>

<div class="row">

    <div class="form-group col-md-2">
        {!! Form::label('cep', 'CEP &nbsp;') !!}
        <a style="font-size: 14px;" target="_blank"
            href="http://www.buscacep.correios.com.br/sistemas/buscacep/buscaCep.cfm">Não sei o
            CEP <i class="fas fa-info-circle"></i></a>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-map-marker-alt"></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control cep', 'maxlength' => '9',
            'placeholder'=> '00000-000', 'name' => 'cep', 'onfocus' =>
            'todasCidades()']) !!}
        </div>
    </div>

    <div class="form-group col-md-2">
        {!! Form::label('uf', 'UF') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-flag"></i>
                </span>
            </div>
            <select name="uf" class="uf form-control">
                <option value=""></option>
            </select>
        </div>
    </div>

    <div class="form-group col-md-3">
        {!! Form::label('cidade', 'Cidade') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-city"></i>
                </span>
            </div>
            <select name="cidade" class="cidade form-control" onfocus="mantemCidade()">
            </select>
        </div>
    </div>

    <div class="form-group col-md-3">
        {!! Form::label('logradouro', 'Logradouro') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-road"></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control logradouro', 'maxlength' => '254',
            'placeholder'=> 'Nome da Rua', 'name' => 'logradouro']) !!}
        </div>
    </div>

    <div class="form-group col-md-2">
        {!! Form::label('numero', 'Número') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-hashtag"></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'placeholder'=> '000',
            'maxlength' => '5', 'name' => 'numero']) !!}
        </div>
    </div>

    <div class="form-group col-md-3">
        {!! Form::label('bairro', 'Bairro') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-map-marked-alt"></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control bairro', 'placeholder'=> 'Bairro',
            'maxlength' => '50', 'name' => 'bairro']) !!}
        </div>
    </div>

    <div class="form-group col-md-4">
        {!! Form::label('complemento', 'Complemento') !!}
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-info"></i>
                </span>
            </div>
            {!! Form::text(null, null, ['class'=>'form-control', 'placeholder'=>
            'Complemento', 'maxlength' => '50', 'name' => 'complemento']) !!}
        </div>
    </div>

</div>