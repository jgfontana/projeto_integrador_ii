@extends('adminlte::page')

@section('title', 'Clientes')

@section('content_header')
<h1>Clientes</h1>
@stop

@section('content')
<div class="container-fluid container-index">
    <div class="row">
        <div class="col">
            <table class="table table-sm table-bordered table-striped" id="table_id">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col" style="text-align: center">Celular</th>
                        <th scope="col" style="text-align: center">Data Nasc.</th>
                        <th scope="col" style="text-align: center">Cidade/UF</th>
                        <th style="width: 10px;"><i style="color: #141646" class="fas fa-eye"></i></th>
                        <th style="width: 10px;"><i style="color: #141646" class="fas fa-user-edit"></i></th>
                        <th style="width: 10px;"><i style="color: #141646" class="fas fa-user-times"></i></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($clientes as $cliente)
                    <tr>
                        <th scope="row"> {{ $cliente->id }} </th>
                        <td>{{ $cliente->pessoa->nome }}</td>
                        <td style="text-align: center">{{ $cliente->pessoa->celular }}</td>
                        <td style="text-align: center">{{ Carbon\Carbon::parse($cliente->pessoa->nasc)->format('d/m/Y')}}</td>
                        <td style="text-align: center">{{ $cliente->pessoa->cidade }} / {{ $cliente->pessoa->uf }}</td>
                        <td style="text-align: center">
                            <a data-toggle="modal" data-target="#modalViewCliente"
                                data-nome="{{ $cliente->pessoa->nome }}"
                                data-nasc="{{ $cliente->pessoa->nasc }}"
                                data-cpf="{{ $cliente->pessoa->cpf }}" data-sexo="{{ $cliente->pessoa->sexo }}"
                                data-celular="{{ $cliente->pessoa->celular }}" data-fone="{{ $cliente->pessoa->fone }}"
                                data-email="{{ $cliente->pessoa->email }}" data-alergia="{{ $cliente->alergia }}"
                                data-descricao="{{ $cliente->descricao }}" data-cep="{{ $cliente->pessoa->cep }}"
                                data-uf="{{ $cliente->pessoa->uf }}" data-cidade="{{ $cliente->pessoa->cidade }}"
                                data-logradouro="{{ $cliente->pessoa->logradouro }}"
                                data-numero="{{ $cliente->pessoa->numero }}"
                                data-bairro="{{ $cliente->pessoa->bairro }}"
                                data-complemento="{{ $cliente->pessoa->complemento }}">
                                <i style="color: black; font-size: 20px;" class="fas fa-eye"></i></td>
                        </a>
                        <td style="text-align: center">
                            <a data-toggle="modal" data-target="#modalEditCliente" data-id="{{ $cliente->id }}"
                                data-nome="{{ $cliente->pessoa->nome }}"
                                data-nasc="{{ $cliente->pessoa->nasc }}"
                                data-cpf="{{ $cliente->pessoa->cpf }}" data-sexo="{{ $cliente->pessoa->sexo }}"
                                data-celular="{{ $cliente->pessoa->celular }}" data-fone="{{ $cliente->pessoa->fone }}"
                                data-email="{{ $cliente->pessoa->email }}" data-alergia="{{ $cliente->alergia }}"
                                data-descricao="{{ $cliente->descricao }}" data-cep="{{ $cliente->pessoa->cep }}"
                                data-uf="{{ $cliente->pessoa->uf }}" data-cidade="{{ $cliente->pessoa->cidade }}"
                                data-logradouro="{{ $cliente->pessoa->logradouro }}"
                                data-numero="{{ $cliente->pessoa->numero }}"
                                data-bairro="{{ $cliente->pessoa->bairro }}"
                                data-complemento="{{ $cliente->pessoa->complemento }}">
                                <i style="color: royalblue; font-size: 20px" class="fas fa-user-edit"></i>
                            </a>
                        </td>
                        <td style="text-align: center">
                            <a onclick="return confirmaExclusao({{$cliente->id}})">
                                <i style="color: darkred; font-size: 20px" class="fas fa-user-times"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<footer style="height: 30px">

</footer>

@include('clientes.create')
@include('clientes.edit')
@include('clientes.view')

@stop

@section('table-delete')
"clientes"
@endsection

@section('footer')
&nbsp;
@endsection

@section('css')
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/clientes/datatables.js')}}"></script>
<script type="text/javascript" src="{{asset('js/clientes/edit.js')}}"></script>
<script type="text/javascript" src="{{asset('js/clientes/view.js')}}"></script>
@stop