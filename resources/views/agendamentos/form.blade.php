<div class="form-row">
    <div class="form-group col-md-9">
        <label>Cliente</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-user"></i>
                </span>
            </div>
            <select class="form-control selectpicker cliente_id" onchange="alertaAlergia(this);" name="cliente_id"
                data-live-search="true" required>
                <option style="font-size: 0px;" selected value="" disabled>Informe o Cliente</option>
                @foreach ($clientes as $cliente)
                <option value="{{ $cliente->id }}" @if($cliente->alergia==true) data-subtext="Alérgica" @endif >
                    {{ $cliente->pessoa->nome }}
                </option>
                @endforeach
            </select>
            <div class="input-group-append add_cliente">
                <button class="btn btn-outline new-client" type="button" data-toggle="modal" data-target="#subModal">
                    <i class="fas fa-plus-circle"></i> <span>Cadastro rápido de cliente</span>
                </button>
            </div>
        </div>
    </div>

    <div class="form-group col-md-3">
        <label>Data</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-calendar "></i>
                </span>
            </div>
            <input required name="data" type="date" class="form-control data">
        </div>
    </div>
</div>

<hr style="margin-top: -20px">

<div class="form-row">
    <div class="form-group col-md-3 col-servico">
        <label>Serviço</label>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text icon-input" id="basic-addon1">
                    <i class="fas fa-handshake "></i>
                </span>
            </div>
            <select name="servico_id[]" class="selectpicker form-control servico" onchange="teste(this, 1); sumPreco();"
                data-live-search="true" required>
                <option style="font-size: 0px;" value="" selected disabled>Informe o Serviço</option>
                {{ $max = 10 }}
                @for ($id = 1; $id <= $max; $id++) @foreach ($servicos as $servico) @if ($servico->id_categoria == $id)
                    @foreach ($servicos as $servico)
                    @if ($servico->id_categoria == $id)
                    <optgroup label=" {{ $servico->nome_categoria }} ">
                        @break
                        @endif
                        @endforeach
                        @foreach ($servicos as $servico)
                        @if ($servico->id_categoria == $id)
                        <option value="{{ $servico->id }}"
                            data-subtext="R$ {{ $servico->preco }} | {{ $servico->duracao }}"">{{ $servico->nome }}</option>
                                @endif
                            @endforeach
                            @break
                        @endif
                    @endforeach
                @endfor
            </select>
        </div>
    </div>

    <div class=" form-group col-md-3 col-profissional">
                            <label>Profissional</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text icon-input" id="basic-addon1">
                                        <i class="fas fa-id-card"></i>
                                    </span>
                                </div>
                                <select name="profissional_id[]" class="form-control profissional1"></select>
                            </div>
        </div>

        <div class="form-group col-md-2">
            <label>Hora Início</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text icon-input" id="basic-addon1">
                        <i class="fas fa-clock"></i>
                    </span>
                </div>
                <input name="hora_inicio[]" value="" type="time" class="form-control hora hora_inicio1" required>
            </div>
        </div>

        <div class="form-group col-md-2">
            <label>Duração</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text icon-input" id="basic-addon1">
                        <i class="fas fa-hourglass-half"></i>
                    </span>
                </div>
                <input type="time" name="duracao[]" class="form-control duracao1" required>
            </div>
        </div>

        <input hidden type="time" class="form-control aux_duracao">
        <input hidden type="number" value="" class="stat">


        <div class="form-group col-md-2 btn-add-hide">
            <button type="button" class="btn btn-sm add" id="btn_add"><i style="color: green" class="fas fa-plus"></i>
                Adicionar
                mais serviços</button>
        </div>

    </div>

    <div id="add_form_serv">

    </div>

    <hr style="margin-top: -20px">
    <div class="form-row">
        <div class="form-group obs-show col-md-9">
            <label>Observação</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text icon-input" id="basic-addon1">
                        <i class="fas fa-exclamation"></i>
                    </span>
                </div>
                <input type="text" name="observacao" class="form-control observacao"
                    placeholder="Informe uma observação aqui..." autocomplete="off">
            </div>
        </div>

        <div class="form-group col-md-3 hide-preco-edit">
            <label>Valor Total</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text icon-input" id="basic-addon1">
                        <i class="fas fa-dollar-sign"></i>
                    </span>
                </div>
                <input readonly disabled type="text" class="form-control preco" placeholder="0,00">
            </div>
        </div>
    </div>