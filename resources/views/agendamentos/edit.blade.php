<div class="modal fade" id="EditAgendamento" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agendamento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route'=>["agendamentos.update"], 'method'=>'put']) !!}
                <input type="hidden" name="id" id="id" value="">
                <input type="hidden" name="atendimento_servico_id" id="atendimento_servico_id" value="">
                @include('agendamentos.form')
                <div class="modal-footer" style="margin-top: -35px">
                    <button id="delete" type="button" onclick="ExclusaoAgendamento();" class="btn btn-primary delete">Excluir</button>
                    {!! Form::submit('Salvar Alterações', ['class'=>'btn btn-primary save', 'id'=>'save']) !!}
                    <button id="fin" type="button" onclick="FinalizaAgendamento();" class="btn btn-primary finalizar_button">Finalizar Atendimento</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>