@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')

@stop

@section('content')
<div class="container-fluid container-index" style="margin-top: -15px">
	<div id='external-events'>
		<div id='external-events-list'></div>
	</div>

	<div class="row">
		<div class="col-md-2">
			<div style="text-align: center">
				<h2><i class="fas fa-fw fa-calendar-alt"></i> Agenda</h2>
			</div>
			<hr>
			<button type="button" data-toggle="modal" data-target="#modalAgenda" class="btn-new btn btn-primary">Novo
				Agendamento</button>
			<hr>
			<h5 style="text-align: center">Filtros</h5>
			<label><i class="fas fa-fw fa-filter"></i>Mostrar agenda de:</label>
			<select id="seleciona" size="{{$count+4}}" class="select-profissional-ag" name="profissional_id"
				style="margin-top: -17px; background-color: transparent">
				<option style="border-bottom: 6px solid grey" value="" selected>Todos</option>
				@foreach ($profissionais as $p)
				<option value="{{$p->id}}" style="border-bottom: 5px solid {{$p->cor}}"> {{$p->nome}} </option>
				@endforeach
			</select>
			<label><i class="fas fa-fw fa-filter"></i>Status:</label>
			<select id="status" class="form-control" style="margin-top: -17px">
				<option value="A" selected>Em aberto</option>
				<option value="F">Finalizado</option>
			</select>
		</div>
		<div class="col-md-10">
			<div class=" card card-primary">
				<div class="card-body p-0">

					<div id='calendar' data-route-load-events="{{ route('routeLoadEvents') }}">

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<footer style="height: 30px">

</footer>

@include('agendamentos.create')
@include('agendamentos.edit')
@include('clientes.create-fast')
@stop
@section('footer')
&nbsp;
@endsection


@section('css')
@stop

@section('js')
<?php $ok = Session::get('ok');  ?>

@if(!empty($ok) )
<script>
	$(function() {
    $('#modalAgenda').modal('show');
    $('#modalAgenda .cliente_id').val({{$ok}});
    $('#modalAgenda .cliente_id').selectpicker('refresh');
});
</script>
@endif

<script>
	$(document).ready(function() {
	//$('#hide_menu').click();
	$('#EditAgendamento .cliente_id').attr("style", "pointer-events: none;");
	$('#EditAgendamento .profissional1').attr("style", "pointer-events: none;");
});
</script>

<script>
	function ExclusaoAgendamento() {
	var atendimento_servico = $('#EditAgendamento #atendimento_servico_id').val();
	Swal.fire({
		title: 'Você tem certeza?',
		text: "Você não poderá reverter isso!!",
		imageUrl: "http://127.0.0.1:8000/vendor/sweetalert2/warning.png",
		backdrop: 'rgba(20,22,70,0.4)',
		imageWidth: 100,
		imageHeight: 100,
		showCancelButton: true,
		closeOnConfirm: false,
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Sim, exclua-o!',
	}).then((result) => {
		if (result.value) {
			$.get('agendamento/delete', {
				atendimento_servico: atendimento_servico
			}, function (data) {
				if (data.status == "ok") {
					Swal.fire(
						'Deletado!',
						'Exclusão confirmada.',
						'success'
					).then(function (isConfirm) {
						window.location.reload();
					});
				} else
					Swal.fire(
						'Erro!',
						'Ocorreram erros na exclusão. Entre em contato com o suporte.',
						'error'
					);
			});
		}
	})
}

function FinalizaAgendamento() {
	var atendimento_servico = $('#EditAgendamento #atendimento_servico_id').val();
	Swal.fire({
		title: 'Finalizar o atendimento?',
		text: "Esta ação não poderá ser revertida!",
		imageUrl: "http://127.0.0.1:8000/vendor/sweetalert2/hands.png",
		backdrop: 'rgba(20,22,70,0.4)',
		imageWidth: 100,
		imageHeight: 100,
		showCancelButton: true,
		closeOnConfirm: false,
		cancelButtonText: 'Cancelar',
		confirmButtonText: 'Sim, finalizar!',
	}).then((result) => {
		if (result.value) {
			$.get('agendamento/finaliza', {
				atendimento_servico: atendimento_servico
			}, function (s) {
				if (s.status == "ok") {
					Swal.fire(
						'Finalizado!',
						'Sucesso, o antendimento foi finalizado!',
						'success'
					).then(function (isConfirm) {
						window.location.reload();
					});
				} else
					Swal.fire(
						'Erro!',
						'Ocorreram erros na finalização!. Entre em contato com o suporte.',
						'error'
					);
			});
		}
	})
}
</script>

<script>
	$(document).ready(function () {
	$('#EditAgendamento .servico').on('change', function () {
		var servico_id = $(this).val();
		if ($.trim(servico_id) != '') {
			$.get('agendamento/profissionais', {
				servico_id: servico_id
			}, function (profissionais) {
				$('#EditAgendamento .profissional1').empty();
				//$('#profissional').append("<option value=''>Selecione o Profissional</option>");
				$.each(profissionais, function (index, value) {
					$('#EditAgendamento .profissional1').append("<option value='" + index + "'>" + value + "</option>");
				})
			});
			$.get('agendamento/servicos', {
				servico_id: servico_id
			}, function (servicos) {
				$.each(servicos, function (duracao, preco) {
					var status = $('#EditAgendamento .stat').val();
					var val = $('#EditAgendamento .aux_duracao').val();
					if (status == 1) {
						$('#EditAgendamento .duracao1').val(val);
						$('#EditAgendamento .stat').val(0);
					} else {
						$('#EditAgendamento .duracao1').val(duracao);
					}
				})
			});
		}
	});
});
</script>

<script>
	$('#modalAgenda').on('show.bs.modal', function (event) {
	$('#modalAgenda .servico').addClass('sum');
	$('#modalAgenda .hora').addClass('check');
})
$('#EditAgendamento').on('show.bs.modal', function (event) {
	$('#EditAgendamento .stat').val(1);
	$('#EditAgendamento .add_cliente').hide();
	$('#EditAgendamento .hide-preco-edit').hide();
    $('#EditAgendamento .obs-show').addClass('col-md-12');
    $('#EditAgendamento .btn-add-hide').hide();
    $('#EditAgendamento .col-servico').addClass('col-md-4');
    $('#EditAgendamento .col-profissional').addClass('col-md-4');
	$('#EditAgendamento .servico').attr('name', 'servico_id');
	$('#EditAgendamento .profissional1').attr('name', 'profissional_id');
	$('#EditAgendamento .hora').attr('name', 'hora_inicio');
	$('#EditAgendamento .duracao1').attr('name', 'duracao');
})
</script>

<script>
	$("#subModal #sair").click(function () {
	$('#subModal').modal('hide')
}); 
</script>

<script>
	$(document).ready(function () {
	var wrapper = $("#modalAgenda #add_form_serv");
	var x = 1;
    mostraHorarios(x);

	$('#modalAgenda #btn_add').click(function (e) {
		var check_exist_servico = {};
		var check_exist_inicio = $('#modalAgenda .check').val();

		$.each($('#modalAgenda .sum'), function () {
			check_exist_servico = $(this).val();
		});

		$.each($('#modalAgenda .check'), function () {
			check_exist_inicio = $(this).val();
		});


		if (check_exist_servico == null || check_exist_inicio == "") {
			Swal.fire('Verifique se os campos Serviço/Profissional/Hora/Duração foram informados corretamente antes de adicionar outro!');
		} else {
			var x_old = x;
            x++;
            var z = x_old+1;
			var newField = '<div class="rem form-row show' + x + '"><div class="form-group col-md-3"> <label>Serviço</label> <div class="input-group mb-3"> <div class="input-group-prepend"> <span class="input-group-text icon-input" id="basic-addon1"> <i class="fas fa-handshake "></i> </span> </div> <select  onchange="teste(this, ' + x + ');  sumPreco();" name="servico_id[]" class="form-control sum servico' + x + '"  data-live-search="true" required> <option style="font-size: 0px;" value="" selected disabled>Informe o Serviço</option> {{ $max = 10 }} @for ($id = 1; $id <= $max; $id++) @foreach ($servicos as $servico) @if ($servico->id_categoria == $id) @foreach ($servicos as $servico) @if ($servico->id_categoria == $id) <optgroup label=" {{ $servico->nome_categoria }} "> @break @endif @endforeach @foreach ($servicos as $servico) @if ($servico->id_categoria == $id) <option value="{{ $servico->id }}" data-subtext="R$ {{ $servico->preco }} | {{ $servico->duracao }}"">{{ $servico->nome }}</option> @endif @endforeach @break @endif @endforeach @endfor </select> </div> </div> <div class=" form-group col-md-3"> <label>Profissional</label> <div class="input-group mb-3"> <div class="input-group-prepend"> <span class="input-group-text icon-input" id="basic-addon1"> <i class="fas fa-id-card"></i> </span> </div> <select name="profissional_id[]" class="form-control profissional profissional' + x + '"></select> </div> </div><div class="form-group col-md-2"> <label>Hora Início</label> <div class="input-group mb-3"> <div class="input-group-prepend"> <span class="input-group-text icon-input" id="basic-addon1"> <i class="fas fa-clock"></i> </span> </div> <input name="hora_inicio[]" type="time" value="" class="form-control check hora_inicio' + x + '" required> </div> </div> <div class="form-group col-md-2"> <label>Duração</label> <div class="input-group mb-3"> <div class="input-group-prepend"> <span class="input-group-text icon-input" id="basic-addon1"> <i class="fas fa-hourglass-half"></i> </span> </div> <input type="time" name="duracao[]" class="form-control duracao' + x + '" required> </div> </div><div class="form-group col-md-2"><button type="button" class="btn btn-sm remove_field"><i style="color: red" class="fas fa-times"></i> Remover Serviço</button></div></div>';
			$(wrapper).append(newField);
			$('.show' + x).hide().show('slow');
			$('.servico' + x).selectpicker();
            mostraHorarios(z);
			jQuery.fn.fadeOutAndRemove = function (speed) {
				$(this).hide(speed, function () {
					$(this).remove();
					sumPreco();
					x--;
				})
			}
		}
		// AO ADICIONAR UM NOVO SERVIÇO, SOMAR A HORA INCIAL ANTERIOR + DURAOAO ANTERIOR PARA PREENCHER AUTOMATICAMENTEA HORA DE INICIO DO NOVO SERVICO ADICIONADO.
		var duracao_anterior = $('#modalAgenda .duracao' + x_old).val();
		var hora_anterior = $('#modalAgenda .hora_inicio' + x_old).val();
		novaHora = somaHora(hora_anterior, duracao_anterior, true);
		$('#modalAgenda .hora_inicio' + x).val(novaHora);
	});

	$(wrapper).on("click", ".remove_field", function (e) {
		e.preventDefault();
		$(this).parents('.rem').fadeOutAndRemove('slow');
	});
})

function teste(id, x) {
	$(x).each(function (index, element) {
		var serv_id = id.value;
		if ($.trim(serv_id) != '') {
			$.get('agendamento/profissionais', {
				servico_id: serv_id
			}, function (profissionais) {
				$('#modalAgenda .profissional' + element).empty();
				//$('#profissional').append("<option value=''>Selecione o Profissional</option>");
				$.each(profissionais, function (index, value) {
					$('#modalAgenda .profissional' + element).append("<option value='" + index + "'>" + value + "</option>");
				})
				$('#modalAgenda .profissional' + element).change();
			});
			$.get('agendamento/servicos', {
				servico_id: serv_id
			}, function (servicos) {
				$.each(servicos, function (duracao, preco) {
					$('#modalAgenda .duracao' + element).val(duracao);
				})
			});
		}
	});
	$('#modalAgenda .hora_inicio' + x).focus();
	$('#modalAgenda .hora_inicio' + x).keypress();
}

function sumPreco() {
	var sum = 0;
	var arrText = new Array();
	var arrText = $('#modalAgenda .sum').map(function () {
		return this.value;
	}).get();
	if ($.trim(arrText) != '') {
		$.get('agendamento/soma-precos', {
			servico_id: arrText
		}, function (soma) {
			$.each(soma, function (index, value) {
				sum += parseFloat(value);
			})
			sum = sum.toFixed(2).replace(".", ",");
			$('#modalAgenda .preco').val(sum);
		});
	}
}

function somaHora(hrA, hrB, zerarHora) {
	if (hrA.length != 5 || hrB.length != 5) return "00:00";

	temp = 0;
	nova_h = 0;
	novo_m = 0;

	hora1 = hrA.substr(0, 2) * 1;
	hora2 = hrB.substr(0, 2) * 1;
	minu1 = hrA.substr(3, 2) * 1;
	minu2 = hrB.substr(3, 2) * 1;

	temp = minu1 + minu2;
	while (temp > 59) {
		nova_h++;
		temp = temp - 60;
	}
	novo_m = temp.toString().length == 2 ? temp : ("0" + temp);

	temp = hora1 + hora2 + nova_h;
	while (temp > 23 && zerarHora) {
		temp = temp - 24;
	}
	nova_h = temp.toString().length == 2 ? temp : ("0" + temp);

	return nova_h + ':' + novo_m;
}
</script>

<script>
	function mostraHorarios(i){
	$("#modalAgenda .hora_inicio" + i)
		.keypress(function () {
			$.ajax({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				type: "POST",
				url: 'load-events',
				success: function (data) {
					var teste = [];
					var dia = $('#modalAgenda .data').val();
					var id = $('#modalAgenda .profissional' + i).val();
					var name = $('#modalAgenda .profissional' + i).text();

					$.each(data, function (index, value) {
						if (value.profissional_id == id && value.data2 == dia && value.status == 'A') {
							teste += '<li>' + moment(value.start).format("HH:mm") + ' - ' + moment(value.end).format("HH:mm") + '  ' + value.title + '</li>';
						}
					})
					var render = "";
					render = teste;
					$('#modalAgenda .hora_inicio' + i).popover('dispose');
					$("#modalAgenda .hora_inicio" + i).popover({
						title: 'Agenda de ' + name + ' em ' + moment(dia).format("DD/MM/YYYY"),
						html: true,
						content: render
					}).popover('show');
				}
			})
		})
		.blur(function () {
			$(this).popover('hide');
        });
    }
</script>

<script>
	function alertaAlergia(id){
    var cli_id = id.value;
    if ($.trim(id) != '') {
        $.get('agendamento/alerta', { cliente_id: cli_id }, function (alerta) {
            $.each(alerta, function (index, value) {  
                if (value == "") {
                    $('#modalAgenda .observacao').val('');
                }else{
                    Swal.fire(
                        'Atenção! Cliente Alérgico(a)',
                        value,
                        'warning'
                    )
                    $('#modalAgenda .observacao').val(value);
                }
            })
        });
    }
}
</script>

<script>
	window.onload = initPage;

function initPage() {
	$("#seleciona").val('').change();
}
</script>
@stop