<div class="modal fade" id="modalAgenda" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Novo Agendamento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route'=>'agendamentos.store']) !!}
                @include('agendamentos.form')
                <div class="modal-footer" style="margin-top: -35px">
                    <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">Cancelar</button>
                    {!! Form::submit('Agendar', ['class'=>'btn btn-primary save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>