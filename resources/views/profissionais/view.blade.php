<div class="modal fade view" id="modalViewProfissional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                @include('profissionais.form')
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary exit" data-dismiss="modal">Ok</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
    
