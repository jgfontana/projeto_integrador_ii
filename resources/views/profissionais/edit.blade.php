<div class="modal fade edit" id="modalEditProfissional" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['route'=>["profissionais.update"], 'method'=>'put']) !!}
                <input type="hidden" name="id" id="id" value="">
                @include('profissionais.form')
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary cancel" data-dismiss="modal">Cancelar</button>
                    {!! Form::submit('Salvar Alterações', ['class'=>'btn btn-primary save']) !!}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>