@extends('adminlte::page')

@section('title', 'Profissionais')

@section('content_header')
<h1>Profissionais</h1>
@stop

@section('content')
<nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
        <a style="color: inherit" class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href=""
            role="tab" aria-controls="nav-home" aria-selected="true"><i class="fas fa-fw fa-id-card"></i> &nbsp;
            Cadastro de Profissionais</a>
        <a style="color: inherit" class="nav-item nav-link" id="nav-profile-tab"
            href=" {{route('profissional-servico')}} " role="tab" aria-controls="nav-profile" aria-selected="false"><i
                class="fas fa-fw fa-user-cog "></i> &nbsp; Serviço exercido por profissional</a>
    </div>
</nav>
<div class="tab-content">
    <div class="container-fluid container-index-nav">
        <div class="row">
            <div class="col">
                <table class="table table-sm table-bordered table-striped" id="table_id">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col" style="text-align: center">Celular</th>
                            <th scope="col" style="text-align: center">Data Nasc.</th>
                            <th scope="col" style="text-align: center">Cidade/UF</th>
                            <th style="width: 10px;"><i style="color: #141646" class="fas fa-eye"></i></th>
                            <th style="width: 10px;"><i style="color: #141646" class="fas fa-user-edit"></i></th>
                            <th style="width: 10px;"><i style="color: #141646" class="fas fa-user-times"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($profissionais as $profissional)
                        <tr>
                            <th scope="row"> {{ $profissional->id }} </th>
                            <td><i style="color: {{$profissional->cor}} "
                                    class="fas fa-circle"></i>&nbsp;&nbsp;{{ $profissional->pessoa->nome }}</td>
                            <td style="text-align: center">{{ $profissional->pessoa->celular }}</td>
                            <td style="text-align: center">
                                {{ Carbon\Carbon::parse($profissional->pessoa->nasc)->format('d/m/Y')}}</td>
                            <td style="text-align: center">{{ $profissional->pessoa->cidade }} /
                                {{ $profissional->pessoa->uf }}</td>
                            <td style="text-align: center">
                                <a data-toggle="modal" data-target="#modalViewProfissional"
                                    data-nome="{{ $profissional->pessoa->nome }}"
                                    data-nasc="{{ $profissional->pessoa->nasc }}"
                                    data-cpf="{{ $profissional->pessoa->cpf }}"
                                    data-sexo="{{ $profissional->pessoa->sexo }}"
                                    data-celular="{{ $profissional->pessoa->celular }}"
                                    data-fone="{{ $profissional->pessoa->fone }}"
                                    data-email="{{ $profissional->pessoa->email }}"
                                    data-cor="{{ $profissional->cor }}" data-cep="{{ $profissional->pessoa->cep }}"
                                    data-uf="{{ $profissional->pessoa->uf }}"
                                    data-cidade="{{ $profissional->pessoa->cidade }}"
                                    data-logradouro="{{ $profissional->pessoa->logradouro }}"
                                    data-numero="{{ $profissional->pessoa->numero }}"
                                    data-bairro="{{ $profissional->pessoa->bairro }}"
                                    data-complemento="{{ $profissional->pessoa->complemento }}">
                                    <i style="color: black; font-size: 20px;" class="fas fa-eye"></i></td>
                            </a>
                            <td style="text-align: center">
                                <a data-toggle="modal" data-target="#modalEditProfissional"
                                    data-id="{{ $profissional->id }}" data-nome="{{ $profissional->pessoa->nome }}"
                                    data-nasc="{{ $profissional->pessoa->nasc }}"
                                    data-cpf="{{ $profissional->pessoa->cpf }}"
                                    data-sexo="{{ $profissional->pessoa->sexo }}"
                                    data-celular="{{ $profissional->pessoa->celular }}"
                                    data-fone="{{ $profissional->pessoa->fone }}"
                                    data-email="{{ $profissional->pessoa->email }}"
                                    data-cor="{{ $profissional->cor }}" data-cep="{{ $profissional->pessoa->cep }}"
                                    data-uf="{{ $profissional->pessoa->uf }}"
                                    data-cidade="{{ $profissional->pessoa->cidade }}"
                                    data-logradouro="{{ $profissional->pessoa->logradouro }}"
                                    data-numero="{{ $profissional->pessoa->numero }}"
                                    data-bairro="{{ $profissional->pessoa->bairro }}"
                                    data-complemento="{{ $profissional->pessoa->complemento }}">
                                    <i style="color: royalblue; font-size: 20px" class="fas fa-user-edit"></i>
                                </a>
                            </td>
                            <td style="text-align: center">
                                <a onclick="return confirmaExclusao({{$profissional->id}})">
                                    <i style="color: darkred; font-size: 20px" class="fas fa-user-times"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<footer style="height: 30px">

</footer>

@include('profissionais.create')
@include('profissionais.edit')
@include('profissionais.view')

@stop

@section('table-delete')
"profissionais"
@endsection

@section('footer')
&nbsp;
@endsection

@section('css')
@stop

@section('js')
<script type="text/javascript" src="{{asset('js/profissionais/datatables.js')}}"></script>
<script type="text/javascript" src="{{asset('js/profissionais/edit.js')}}"></script>
<script type="text/javascript" src="{{asset('js/profissionais/view.js')}}"></script>
@stop